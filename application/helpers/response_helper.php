<?php defined('BASEPATH') or exit('No direct script access allowed');


if (!function_exists('rspError')) {

    function rspError($msj, $code = 500)
    {
        if (LOG_RSP_ERRORS) log_message("ERROR", "SYS | RSP_ERRORS | $code | $msj");
        http_response_code($code);
        echo json_encode(['status' => false, 'mensaje' => $msj]);
        exit(0);
    }
}

if (!function_exists('rspOk')) {
    function rspOk($msj = '', $data = false)
    {
        if (LOG_RSP_OK) log_message("DEBUG", "SYS | RSP_OK | $msj |" . $data ? json_encode($data, JSON_PRETTY_PRINT) : '');
        if ($data) $data = cifrarId($data);
        echo json_encode(['status' => true, 'mensaje' => $msj, 'data' => $data]);
        exit(0);
    }
}
