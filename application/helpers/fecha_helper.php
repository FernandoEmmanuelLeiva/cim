<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('formato_fecha')) {
    function formato_fecha_hora($fecha)
    {
        if (strlen($fecha) == 0) {
            return '';
        }

        $aux = explode(" ", $fecha);
        if (sizeOf($aux) == 2) {
            $date = explode("-", $aux[0]);
            $date = $date[2] . '/' . $date[1] . '/' . $date[0] . '  -  ' . substr($aux[1], 0, 5) . ' hs';
            return $date;
        }
    }

    function fecha_hora($fecha)
    {
        if (strlen($fecha) == 0) {
            return '';
        }

        $aux = explode(" ", $fecha);
        if (sizeOf($aux) == 2) {
            $date = explode("-", $aux[0]);
            $date = $date[2] . '/' . $date[1] . '/' . $date[0] . '  -  ' . substr($aux[1], 0, 5) . ' hs';
            return $date;
        }
    }

    function fecha($fecha)
    {
        if (strlen($fecha) == 0) {
            return '';
        }

        $fecha = substr($fecha, 0, 10);

        $date = explode("-", $fecha);
        $date = $date[2] . '/' . $date[1] . '/' . $date[0];
        return $date;
    }

    function rfecha($fecha)
    {
        if (strlen($fecha) == 0) {
            return '';
        }

        $date = explode("/", $fecha);
        $date = $date[2] . '-' . $date[1] . '-' . $date[0];
        return $date;
    }

    function restarTiempo($fecha, $horas = 0, $minutos = 0)
    {
        return date('Y-m-d H:i', strtotime("-$horas hour -$minutos minute", strtotime($fecha)));
    }

    function restarFechas($a, $b)
    {
        $fecha1 = new DateTime($a);
        $fecha2 = new DateTime($b);
        $resultado = $fecha1->diff($fecha2);
        $dias = $resultado->format('%a');
        $horas = $resultado->format('%H');
        $minutos = $resultado->format('%i');
        return toMin((intval($dias) * 24) + intval($horas) . ":$minutos");
    }

    function formatFechaPG($fecha)
    {
        $fecha = substr($fecha, 0, 10);
        return fecha($fecha);
    }

    function sumarMeses($fecha, $meses)
    {
        return date('Y-m-d', strtotime("+$meses month", strtotime($fecha)));
    }

    function sumarDias($fecha, $dias)
    {
        return date('Y-m-d', strtotime("+$dias day", strtotime($fecha)));
    }
    if (!function_exists('toMin')) {
        function toMin($tiempo)
        {
            $aux = explode(':', $tiempo);
            $h = intval($aux[0]);
            $min = ($h ? $h * 60 : 0);
            return intval($aux[1]) + intval($min);
        }
    }
    if (!function_exists('sumarTiempo')) {
        function sumarTiempo($fecha, $min)
        {
            $fecha = date($fecha);
            $nuevafecha = strtotime("+$min minute", strtotime($fecha));
            return date('Y-m-d H:i', $nuevafecha);
        }
    }

    if (!function_exists('calcularEdad')) {
        function calcularEdad($fecha)
        {
            $fecha = date($fecha);
            $hoy = new DateTime();
            $edad = $hoy->diff(new DateTime($fecha));
            return $edad->y;
        }
    }
}
