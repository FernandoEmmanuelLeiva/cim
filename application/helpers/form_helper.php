<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('form')) {
    function form($id, $campos, $button = '')
    {
        $ci = &get_instance();
        $readonly = strpos($id, '-v');
        if (!$campos) return 'Sin formulario';
        $html = "<form id='$id' method='post' action='" . base_url(reset($campos)->post_url) . "'><div class='row'><div class='col-md-12'><p class='frm-msj text-danger text-center'></p></div>";
        foreach ($campos as $o) {
            switch ($o->tipo) {
                case 'title':
                    $html .= "<div class='col-md-$o->col'><h1 class='lead'>$o->label</h1></div>";
                    break;
                case 'text':
                    $html .= fgi($o, $readonly);
                    break;
                case 'number':
                    $html .= fgn($o, $readonly);
                    break;
                case 'file':
                    $html .= fgn($o, $readonly);
                    break;
                case 'radio':
                    $html .= radio($o);
                    break;
                case 'checkbox':
                    $html .= checkbox($o);
                    break;
                case 'date':
                    $html .= fgd($o);
                    break;
                case 'select':
                    $html .= fgs($o);
                    break;
                case 'select_html':
                    $html .= fgs($o, $ci->load->view($o->valores, null, true));
                    break;
                case 'button':
                    $html .= fbutton($o);
                    break;
                case 'view':
                    $html .= $ci->load->view($o->valores, null, true);
                    break;
                default:
                    # code...
                    break;
            }
        }
        return "$html</div>$button</form>";
    }
}

if (!function_exists('fgi')) {
    function fgi($o, $read = false)
    {
        if (is_array($o)) {
            $o = toStd($o);
        }

        if (strlen($o->icon)) {

            return "<div class='col-md-" . (isset($o->col) ? $o->col : "12") . " " . (isset($o->class) ? $o->class : '') . "'>
                    <div class='input-group mb-3'>
                    <div class='input-group-prepend'>
                    <span class='input-group-text'><i class='fas $o->icon'></i></span>
                    </div>
                    <input " . ($read ? 'disabled' : '') . " type='" . (isset($o->type) ? $o->type : 'text') . "' class='form-control' placeholder='" . (isset($o->label) ? $o->label : ucfirst($o->name)) . "' name='$o->name' id='$o->name'" . (($o->required) ? 'required' : '') . ">
                    </div>
                    </div>";
        } else {
            return "<div class='col-" . (isset($o->col) ? $o->col : "12") . "'>
                        <div class='form-group'>
                            <label>" . (isset($o->label) ? $o->label : ucfirst($o->name)) . ":</label>
                            <input type='" . (isset($o->type) ? $o->type : 'text') . "' class='form-control' name='$o->name' id='$o->name'" . (($o->required) ? 'required' : '') . " />
                        </div>
                    </div>";
        }
    }
}

if (!function_exists('fgd')) {
    function fgd($o, $read = false)
    {
        if (is_array($o)) {
            $o = toStd($o);
        }

        if (strlen($o->icon)) {

            return "<div class='col-md-" . (isset($o->col) ? $o->col : "12") . " " . (isset($o->class) ? $o->class : '') . "'>
            <div class='input-group mb-3'>
            <div class='input-group-prepend'>
            <span class='input-group-text'><i class='fas $o->icon'></i></span>
            </div>
            <input " . ($read ? 'disabled' : '') . " type='date' class='form-control date' placeholder='" . (isset($o->label) ? $o->label : ucfirst($o->name)) . "' name='$o->name' id='$o->name'" . (($o->required) ? 'required' : '') . ">
            </div>
            </div>";
        } else {
            return "<div class='col-" . (isset($o->col) ? $o->col : "12") . "'>
                        <div class='form-group'>
                            <label>" . (isset($o->label) ? $o->label : ucfirst($o->name)) . ":</label>
                            <input type='date' class='form-control' name='$o->name' id='$o->name'" . (($o->required) ? 'required' : '') . " />
                        </div>
                    </div>";
        }
    }
}

if (!function_exists('fgn')) {
    function fgn($o, $read = false)
    {
        if (is_array($o)) {
            $o = toStd($o);
        }
        return "<div class='col-md-$o->col " . (isset($o->class) ? $o->class : '') . "'>
                    <div class='input-group mb-3'>
                        <div class='input-group-prepend'>
                            <span class='input-group-text'><i class='fas $o->icon'></i></span>
                        </div>
                        <input " . ($read ? 'disabled' : '') . " type='number' class='form-control' placeholder='" . ($o->label ? $o->label : ucfirst($o->name)) . "' name='$o->name' id='$o->name'" . ($o->required ? 'required' : '') . ">
                    </div>
                </div>";
    }
}

if (!function_exists('fgs')) {
    function fgs($o, $html = '')
    {
        if (is_array($o)) {
            $o = toStd($o);
        }
        return "<div class='col-md-" . (isset($o->col) ? $o->col : "12") . " " . (isset($o->class) ? $o->class : '') . "'>
                    <div class='input-group mb-3'>
                        <div class='input-group-prepend'>
                            <span class='input-group-text'><i class='fas $o->icon'></i></span>
                        </div>
                        <select  class='form-control' name='$o->name' id='$o->name'" . ($o->required ? 'required' : '') . "><option selected disabled>- " . (isset($o->label) ? $o->label : ucfirst($o->name)) . " -</option></i>$html</select>
                    </div>
                </div>";
    }
}

if (!function_exists('radio')) {
    function radio($o)
    {
        if (is_array($o)) {
            $o = toStd($o);
        }
        if (sizeof($o->valores)) {
            $html = "<div class='col-md-$o->col'><label>$o->label:</label><div class='form-group '>";
            foreach ($o->valores as $key => $e) {
                $html .= "<div class='custom-control custom-radio ml-4'>
                <input class='custom-control-input' type='radio' id='customRadio$key' name='$o->name' value='" . json_encode([$e->descripcion => $e->valor]) . "' " . ($o->required ? 'required' : '') . ">
                <label for='customRadio$key' class='custom-control-label'>$e->descripcion</label>
                </div>";
            }
            return "$html</div>";
        } else {
            return '';
        }
    }
}

if (!function_exists('checkbox')) {
    function checkbox($o)
    {
        if (is_array($o)) {
            $o = toStd($o);
        }
        if (sizeof($o->valores)) {
            $html = "<div class='col-$o->col'><h1 class='lead'>$o->label</h1></div><div class='form-group ml-5'>";
            foreach ($o->valores as $key => $e) {
                $html .= "<div class='custom-control custom-checkbox'>
                <input class='custom-control-input' type='checkbox' id='customcheckbox$key' name='$o->name' value='" . json_encode([$e->descripcion => $e->valor]) . "'>
                <label for='customcheckbox$key' class='custom-control-label'>$e->descripcion</label>
                </div>";
            }
            return "$html</div>";
        } else {
            return '';
        }
    }
}

if (!function_exists('fbutton')) {
    function fbutton($o)
    {
        $o = toStd($o);
        return "<div class='col-md-$o->col'><button id='$o->name' class='$o->class'><i class='$o->icon mr-2'></i>$o->label</button></div>";
    }
}

if (!function_exists('validarCampos')) {
    function validarCampos($data, $values = false)
    {
        if ($values) {
            foreach ($values as $o) {
                if (isset($data[$o]) && $data[$o] != "") {
                    continue;
                }
                log_message('ERROR', 'SYS_ERROR | ' . __METHOD__ . " | Empty ==> $o");
                return false;
            }
        } else {
            foreach ($data as $key => $o) {
                if ($o && $o != '') {
                    continue;
                }

                return false;
            }
        }
        return true;
    }
}

if (!function_exists('getForm')) {
    function getForm($id, $formId, $instId = false)
    {
        $ci = &get_instance();
        if ($instId) {
            $campos = $ci->Form->obtenerInstancia($instId);
        } else {

            $campos = $ci->Form->obtenerXNombre($formId);
        }
        $form = form($id, $campos);
        return $form;
    }
}

if (!function_exists('fi')) {
    function fi($o)
    {
        if (is_array($o)) {
            $o = toStd($o);
        }

        return "<div class='col-" . (isset($o->col) ? $o->col : "12") . "'>
                    <div class='form-group'>
                        <label>" . (isset($o->label) ? $o->label : ucfirst($o->name)) . ":</label>
                        <input type='" . (isset($o->type) ? $o->type : 'text') . "' class='form-control' name='$o->name' id='$o->name'" . (($o->required) ? 'required' : '') . " />
                    </div>
                </div>";
    }
}
