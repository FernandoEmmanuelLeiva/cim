<?php defined('BASEPATH') or exit('No direct script access allowed');

use Hashids\Hashids;

if (!function_exists('encToken')) {

    function encToken($id)
    {
        $hashids = new Hashids('', 20, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        $token  = date('YmdHi').$id;
        return $hashids->encode($token);
    }
}

if (!function_exists('decToken')) {

    function decToken($token)
    {
        $hashids = new Hashids('', 20, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        $res = $hashids->decode($token);
        $token = strval(reset($res));
        $date = substr($token, 0, 12);
        if(restarFechas($date, date('Y-m-d H:i')) < TIME_EXPIRED_TOKEN){
            $id = str_replace($date, "", $token);
            if(strlen($id)) return $id;
        }else{
            msjPage('Ups!','Token Inválido/Vencido','fa fa-times-circle text-danger','Entendido', base_url());
        }
    }
}

if (!function_exists('infoToken')) {

    function infoToken($token)
    {
        $token = decToken($token);
        if($token)
        {
            $ci = &get_instance();
            $ci->load->model('Token');    
            $token = $ci->Token->obtener($token, true);
            if($token){
                return json_decode($token->data_json);
            }else{
                msjPage('Ups!','Token Inválido/Vencido','fa fa-times-circle text-danger','Entendido', base_url());
            }
        }
    }
}


if(!function_exists('enc'))
{
    
    function enc($data)
    {
        $hashids = new Hashids('', 10, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

        return $hashids->encode($data);
       # return hashids_encrypt($data, "1234567", 10);
    }
}

if(!function_exists('dec'))
{
    function dec($data)
    {
        $hashids = new Hashids('', 10, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        $output = $hashids->decode($data);
        if(count($output) < 1) return NULL;
        return (count($output) == 1) ? reset($output) : $output;
    }
}

function eurl($url)
{
    return "1".implode(array_map(function ($n) { return sprintf('%03d', $n); },
    unpack('C*', $url)));
}

function durl($url)
{
    return implode(array_map('chr', str_split(substr($url, 1), 3)));
}

function baseurl($url = '', $enc = false)
{
    if($enc) $url = 'api/'.eurl($url);
    return base_url($url);
}


if (!function_exists('diffFechas')) {

    function diffFechas($desde, $hasta)
    {
        return round(abs(strtotime($desde) - strtotime($hasta)) / 60, 2);
    }
}


if(!function_exists('cifrarIds'))
{
    function cifrarIds($data, $id = false)
    {
        $data = toArray($data);

        if($id){
            #SI ES UN SOLO OBJETO
            if(isset($data[$id])){
                $data[$id] = enc($data[$id]);
            }else{
                #SI ES UN ARREGLO DE OBJETOS
                foreach ($data as $key => $o) {
                    $data[$key][$id] = enc($o[$id]);
                }
            }
        }else{
            foreach ($data as $key => $o) {
                if(strpos($key,'_id')) $data[$key] = enc($o);
            }
        }
            
        return  toStd($data);
    }
}

if(!function_exists('cifrarId'))
{
    function cifrarId($data)
    {
        $data = toArray($data);

        foreach ($data as $key => $o) {
            if(strpos($key,'_id')) $data[$key] = enc($o);
        }
            
        return  toStd($data);
    }
}