<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('btnLink')) {

    function btnLink($text, $link, $class = 'btn-admin')
    {
        return "<a href='$link' class='btn btn-link $class'>$text</a>";
    }

}
