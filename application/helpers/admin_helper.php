<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('card')) {
    function card($color, $titulo, $subtitulo, $icon, $link = false)
    {
        return "<div class='small-box bg-$color'>
            <div class='inner'>
                <h3>$titulo</h3>
                <p>$subtitulo</p>
            </div>
            <div class='icon'>
                <i class='ion ion-$icon'></i>
            </div>
            <a " . ($link ? '' : '') . " href='#' class='small-box-footer'>More info <i class='fas fa-arrow-circle-right'></i></a>
        </div>";
    }
}


if (!function_exists('alert')) {
    function alert($titulo, $msj, $icono, $color = false)
    {
        $color = $color ? "alert-$color" : 'bg-light';
        return "<div class='alert $color'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    <h5><i class='icon $icono'></i>$titulo</h5>
                    $msj
                </div>";
    }
}


if (!function_exists('comp')) {

    function comp($id, $url, $load = false)
    {
        return "<comp id='$id' class='reload' data-link='$url'>" . ($load ? "<script>reload('#$id','$load')</script>" : '') . "</comp>";
    }
}

if (!function_exists('estado')) {

    function estado($estado)
    {
        switch ($estado) {
            case 'incompleto':
                return "<small class='estado estado-$estado badge bg-warning text-capitalize'>$estado</small>";
                break;
            case 'completo':
                return "<small class='estado estado-$estado badge bg-success text-capitalize'>$estado</small>";
                break;

            default:
                # code...
                break;
        }
    }
}

if (!function_exists('msjPage')) {
    function msjPage($titulo, $msj, $icon, $btn = false, $btnLink = false)
    {
        $ci = &get_instance();
        $data = array(
            'titulo' => $titulo,
            'msj' => $msj,
            'iconClass' => $icon,
            'btn' => $btn,
            'btnLink' => $btnLink,
            'view' => 'layout/mensaje'
        );
        $ci->load->view('layout/singlePage', $data);
    }
}

if(!function_exists('pageError')){
    function pageError($title, $description, $link = false){
        return msjPage($title, $description, 'fa fa-times text-danger');
    }
}

if (!function_exists('msjPane')) {
    function msjPane($titulo, $msj, $icon, $btn = false, $btnLink = false)
    {
        $ci = &get_instance();
        $data = array(
            'titulo' => $titulo,
            'msj' => $msj,
            'iconClass' => $icon,
            'btn' => $btn,
            'btnLink' => $btnLink
        );
        $ci->load->view('layout/mensaje', $data);
    }
}

if (!function_exists('bolita')) {
    function bolita($txt, $color = 'secondary')
    {
        return "<span class='estado badge badge-$color'>$txt</span>";
    }
}

if (!function_exists('opciones')) {
    function opciones($opts)
    {
        $html = "<button type='button' class='btn btn-link' data-toggle='dropdown' aria-expanded='false'><i
        class='fas fa-ellipsis-v text-gray'></i>
        </button>
        <div class='dropdown-menu' role='menu' style=''>";
        foreach ($opts as $key => $o) {
            $html .= "<a class='dropdown-item' href='#' onclick='$o'>$key</a>";
        }
        return "$html</div>";
    }
}

if (!function_exists('boxCard')) {
    function boxCard($html)
    {
        return "<div class='login-box text-center'>
                    <div class='login-logo'>
                        <a href='#'>" . TITULO_LOGIN . "</a>
                    </div>
                    <div class='card'>
                        <div class='card-body'>
                                $html
                        </div>
                    </div>
                </div>";
    }
}


if (!function_exists('tablaVacia')) {
    function tablaVacia($span = 2)
    {
        echo "<tfoot>
                    <tr>
                        <td class='text-center text-gray' colspan='$span'><h5>Tabla vacía</h5></td>
                    </tr>
                </tfoot>";
    }
}


if (!function_exists('dash')) {
    function dash($view, $data = false)
    {
        $ci  = &get_instance();
        $xdata['view'] = $ci->load->view($view, $data, true);
        $ci->load->view('layout/admin', $xdata);
    }
}

if (!function_exists('dashMenu')) {
    function dashMenu($view, $data = false)
    {
        $ci  = &get_instance();
        $xdata['view'] = $ci->load->view($view, $data, true);
        $ci->load->view('layout/admin_menu', $xdata);
    }
}


if (!function_exists('acciones')) {
    function acciones($opt, $more = [])
    {
        $html =
            "<button type='button' class='btn btn-default btn-sm' data-toggle='dropdown'>
            <i class='fas fa-ellipsis-v'></i>
        </button>
        <div class='dropdown-menu' style=''>
        ";

        if ($more) {
            foreach ($more as $key => $o) {
                $html .=  "<a class='dropdown-item $key text-capitalize' href='#'>$o</a>";
            }
            $html .= '<div class="dropdown-divider"></div>';
        }

        if (strpos($opt, 'E') !== false) {
            $html .= "<a class='dropdown-item btn-editar text-capitalize' href='#'>Editar</a>";
        }

        if (strpos($opt, 'D')) {
            $html .= "<a class='dropdown-item btn-eliminar text-capitalize' href='#'>Eliminar</a>";
        }
        return "$html</div>";
    }
}

if (!function_exists('modal')) {
    function modal($id, $title, $content, $btn = '', $body = false)
    {
        return
            "<div class='modal ' id='$id' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
            <div class='modal-content'>
                <div class='modal-header'>
                <h5 class='modal-title' id='exampleModalLabel'>$title</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
                </div>
                <div class='" . (!$body ? 'modal-body' : '') . "'>
                    $content
                </div>
                <div class='modal-footer justify-content-between'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>
                $btn
                </div>
            </div>
            </div>
        </div>";
    }
}

if (!function_exists('selectData')) {
    function selectData($id, $value, $label, $data)
    {
        $html = '<option selected disabled> - Seleccionar - </option>';
        $enc = strpos($value, '_id') !== false;
        log_message('DEBUG', "#ENC | $enc");
        foreach ($data as $o) {
            $html .= "<option value='" . ($enc ? enc($o->{$value}) : $o->{$value}) . "'>" . $o->{$label} . "</option>";
        }
        log_message('DEBUG', "#CULO | $html");
        return "<select class='form-control' id='$id' name='$id'>$html</select>";
    }
}

if (!function_exists('selectOpts')) {
    function selectOpts($data, $value = 'valor', $label = "descripcion", $class = false, $parentId = '')
    {
        if (!$data) return;
        $html = '<option selected disabled value="">- Seleccionar -</option>';
        foreach ($data as $o) {
            $html .= "<option class='$parentId" . ($class?(enc($o->{$class})):'') . "' value='" . enc($o->{$value}) . "'>" . $o->{$label} . "</option>";
        }
        return $html;
    }
}



if (!function_exists('msjError')) {
    function msjError($msj = ERROR_ASM)
    {
        echo $msj;
    }
}

if (!function_exists('tag')) {
    function tag($txt, $color = 'secondary')
    {
        return  "<small class='badge badge-$color tag mr-1'>$txt</small>";
    }
}

if (!function_exists('ic')) {
    function ic($icon)
    {
        return  "<i class='fas fa-$icon mr-1'></i>";
    }
}
