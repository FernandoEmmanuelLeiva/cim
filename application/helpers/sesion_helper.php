<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('sesion')) {
    function sesion($data = false)
    {
        $ci = &get_instance();
        if ($data) {
            $data = toArray($data);
 
            #Validar si hay variables nulas
            foreach ($data as $key => $o) {
                if(!$o) unset($data[$key]);
            }

            $ci->session->set_tempdata($data, null, TIME_EXPIRED_SESSION);
        } else {
            return $ci->session->tempdata();
        }
    }
}

if (!function_exists('sData')) {
    function sData($prop)
    {
        $s = sesion();
        if ($s && isset($s[$prop])) {
            return $s[$prop];
        }else return false;
    }
}

if (!function_exists('rolId')) {
    function rolId()
    {
        if(ROLE) return ROLE;
        else
        return sData('rol_id');
    }
}

if (!function_exists('checkRol')) {
    function checkRol($roles)
    {
        $rol = rolId();
        return $rol? strpos($rol, $roles) !== FALSE: FALSE;
    }
}


if (!function_exists('userId')) {
    function userId()
    {
        return sData('user_id');
    }
}

if (!function_exists('persId')) {
    function persId()
    {
        return sData('pers_id');
    }
}

// if (!function_exists('instId')) {
//     function instId()
//     {
//         return 2;
//     }
// }

if (DEV && !function_exists('certId')) {
    function certId()
    {
        return 1;
    }
}
