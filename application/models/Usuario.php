<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuario extends CI_Model
{
    private $tabla = array(
        'key' => 'user_id',
        'auto_key' => false,
        'tabla' => 'usuarios',
        'bool_delete' => false  
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function asociarInstituto($userId, $instId)
    {
        $data['inst_id'] = $instId;
        $this->db->where('user_id', $userId);
        return $this->db->update('rel_usuarios_roles', $data);
    }

    public function iniciarSesion($email, $password)
    {
       
        $this->db->select('T.user_id, email, estado, RUR.rol_id, pers_id');
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        $this->db->join('rel_usuarios_roles RUR', 'RUR.user_id = T.user_id', 'left');
        $res = $this->GModel->obtenerTodas($this->tabla);
        if(sizeof($res)) return reset($res);
    }

    public function guardarRegistro($email, $password)
    {
        $data['email'] = $email;
        $data['password'] = md5($password);
        $data['estado'] = 'VERIFICACION_EMAIL';
        return $this->GModel->guardar($data, $this->tabla);
    }

    public function obtenerXEmail($email)
    {
        $this->db->where('email', $email);
        $res = $this->GModel->obtenerTodas($this->tabla);
        if(sizeof($res)) return reset($res);
    }

    public function obtenerTodas()
    {
        $this->db->select('user_id, email, estado');
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtener($userId)
    {
        $this->db->select('user_id, email, estado, pers_id');
        return $this->GModel->obtener($userId, $this->tabla);
    }

    public function habilitar($userId, $rol = 'USUARIO')
    {
        $data['estado'] = 'ACTIVO';
        $this->GModel->editar($userId, $data, $this->tabla);
        return $this->db->insert('rel_usuarios_roles', ['user_id'=> $userId, 'rol_id' => $rol]);
    }
    
    public function blanquearClave($userId)
    {
        $data['estado'] = EMAIL_RECUPERACION;
        $data['password'] = '';
        return $this->GModel->editar($userId, $data, $this->tabla);
    }

    public function cambiarClave($userId, $password)
    {
        log_message('ERROR', __METHOD__.json_encode($password));
        $data['estado'] = AC;
        $data['password'] = md5($password);
        return $this->GModel->editar($userId, $data, $this->tabla);
    }

    public function editar($id, $data)
    {
        return $this->GModel->editar($id, $data, $this->tabla);
    }

    public function obtenerRolesInstitutos($userId)
    {
        $this->db->where('user_id', $userId);
        return $this->db->get('rel_usuarios_roles')->first_row();
    }
}
