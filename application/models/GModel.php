<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function obtenerTodas($DTabla)
    {
        $res = $this->db->get($DTabla['tabla']. ' as T');
        return $res->result();
    }

    public function obtener($id, $DTabla)
    {
        if ($id) {
            $this->db->where('T.'.$DTabla['key'], $id);
        }

        if ($DTabla['bool_delete']) {
            $this->db->where('T.delete', false);
        }

        $res = $this->db->get($DTabla['tabla']. ' as T');

        if ($id) {
            return $res->first_row();
        }

        return $res->result();
    }

    public function guardar($data, $DTabla)
    {
        $res = $this->db->insert($DTabla['tabla'], $data);
        if($res){
            if($DTabla['auto_key']) return $this->db->insert_id();
            else return true;
        }
    }

    public function editar($id, $data, $DTabla)
    {
        if (!$id) {
            return false;
        }

        $this->db->where($DTabla['key'], $id);
        return $this->db->update($DTabla['tabla'], $data);
    }

    public function eliminar($id, $DTabla)
    {
        if (!$id) {
            return false;
        }

        if ($DTabla['bool_delete']) {
            $this->db->where($DTabla['key'], $id);
            $this->db->set('delete', true);
            return $this->db->update($DTabla['tabla']);
        } else {
            $this->db->where($DTabla['key'], $id);
            return $this->db->delete($DTabla['tabla']);
        }
    }

}
