<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Instituto extends CI_Model
{
    private $tabla = array(
        'key' => 'inst_id',
        'auto_key' => true,
        'tabla' => 'institutos',
        'bool_delete' => true  
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function obtenerTodas()
    {
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtener($instId)
    {
        $inst = $this->GModel->obtener($instId, $this->tabla);
        if($inst)
        {
            $this->load->model('Pais');
            $inst->pais = $this->Pais->obtenerXId($inst->pais);
            $inst->provincia = $this->Pais->obtenerProvinciaXId($inst->provincia);
            return $inst;
        }
    }


    public function obtenerXCertamen($certId)
    {
        $this->db->where('cert_id', $certId);
        $this->db->join('rel_certamenes_institutos RCI', 'RCI.inst_id = T. inst_id');
        return $this->GModel->obtenerTodas($this->tabla);
    }


    public function obtenerXDni($dni)
    {
        $this->db->select('nombre');
        $this->db->where('dni_director', $dni)  ;
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtenerXUserId($userId)
    {
        $this->db->from('rel_usuarios_roles UR');
        $this->db->join('institutos I', 'I.inst_id = UR.inst_id');
        $this->db->where('UR.user_id', $userId);
        return $this->db->get()->first_row();
    }

    public function obtenerXDirectorId($persId)
    {
        $this->db->where('director_id', $persId);
        $this->db->where('delete', false);
        return $this->db->get('institutos')->first_row();
    }

    public function obtenerInfo($instId)
    {
        $this->db->select('T.nombre, T.alias, T.estado,T.telefono, T.email, T.direccion');
        $this->db->select('PE.nombre as nombre_director, PE.apellido, PE.dni');
        $this->db->select('PA.id as pais, EST.id as provincia, PA.nombre as npais, EST.nombre as nprovincia, T.localidad');
        $this->db->join('personas PE','PE.dni = T.director_id', 'left');
        $this->db->join('pais PA','PA.id = T.pais', 'left');
        $this->db->join('estado EST', 'EST.id = T.provincia', 'left');
        return $this->GModel->obtener($instId, $this->tabla);
    }

    public function obtenerMaestros($certId, $instId)
    {
        // $this->db->select('P.*');
        // $this->db->where('RCIP.cert_id', $certId);
        // $this->db->where('RCIP.inst_id', $instId);
        // $this->db->from('rel_certamenes_institutos_personas RCIP');
        // $this->db->join('personas P','P.dni = RCIP.dni');
        // return $this->db->get()->result();

        $this->db->where('RP.inst_id', $instId);
        $this->db->where('P.maestro_preparador', true);
        $this->db->join('rel_personas_institutos RP', 'RP.pers_id = P.pers_id');
        return $this->db->get('personas P')->result();
    }

    public function guardar($data)
    {
        return $this->GModel->guardar($data, $this->tabla);
    }

    public function editar($instId, $data)
    {
        return $this->GModel->editar($instId, $data, $this->tabla);
    }

}
