<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obra extends CI_Model
{
    private $tabla = array(
        'key' => 'obra_id',
        'auto_key' => true,
        'tabla' => 'obras',
        'bool_delete' => false
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function obtener($obraId)
    {
        $this->db->select('T.*');
        $this->db->select('M.moda_id');
        $this->db->join('estilos E', 'E.esti_id = T.esti_id');
        $this->db->join('modalidades M', 'M.moda_id = E.moda_id');
        return $this->GModel->obtener($obraId, $this->tabla);
    }

    public function obtenerXInstituto($certId, $instId)
    {
        $this->db->select('T.*');
        $this->db->select('C.nombre as categoria');
        $this->db->select('FP.nombre as fopa, FP.precio');
        $this->db->select('M.nombre as modalidad');
        $this->db->select('E.nombre as estilo');
        $this->db->where('cert_id', $certId);
        $this->db->where('inst_id', $instId);
        $this->db->join('formas_participacion FP', 'FP.fopa_id = T.fopa_id');
        $this->db->join('categorias C', 'C.cate_id = T.cate_id');
        $this->db->join('estilos E', 'E.esti_id = T.esti_id');
        $this->db->join('modalidades M', 'M.moda_id = E.moda_id');
        $this->db->order_by('nombre_obra');
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtenerCategorias()
    {
        $this->db->where('delete', false);
        return $this->db->get('categorias')->result();
    }

    public function obtenerFormasParticipacion()
    {
        $this->db->where('delete', false);
        return $this->db->get('formas_participacion')->result();
    }

    public function obtenerModalidades()
    {
        return $this->db->get('modalidades')->result();
    }

    public function obtenerEstilos()
    {
        return $this->db->get('estilos')->result();
    }

    public function obtenerParticipantes($obraId)
    {
        $this->load->model('Persona');

        $this->db->select('T.*');
        $this->db->where('RPO.obra_id', $obraId);
        $this->db->join('rel_obras_personas RPO', 'RPO.pers_id = T.pers_id');
        return $this->Persona->obtenerTodas();
    }

    public function obtenerCategoria($cateId)
    {
        $this->db->where('cate_id', $cateId);
        $res = $this->obtenerCategorias();
        if ($res) return reset($res);
    }

    public function agregarParticipante($obraId, $dni)
    {
        $res = $this->db->insert('rel_obras_personas', ['obra_id' => $obraId, 'pers_id' => $dni]);
        if($res) $this->actualizarEstadoObra($obraId);
        return $res;
    }

    public function guardar($data)
    {
        return $this->GModel->guardar($data, $this->tabla);
    }

    public function editar($obraId, $data)
    {
        return $this->GModel->editar($obraId, $data, $this->tabla);
    }

    public function eliminar($obraId)
    {
        $this->eliminarParticipantes($obraId);
        return $this->GModel->eliminar($obraId, $this->tabla);
    }

    public function eliminarParticipantes($obraId)
    {
        $this->db->where('obra_id', $obraId);
        $res = $this->db->delete('rel_obras_personas');
        if($res) $this->actualizarEstadoObra($obraId);
        return $res;
    }

    public function eliminarParticipante($obraId, $persId)
    {
        $this->db->where('pers_id', $persId);
        $res = $this->eliminarParticipantes($obraId);
        if($res) $this->actualizarEstadoObra($obraId);
        return $res;
    }

    public function actualizarEstadoObra($obraId)
    {
        //Obtener datos de obra
        $obra =  $this->obtener($obraId);

        //Obtener forma de participacion
        $this->db->where('fopa_id', $obra->fopa_id);
        $fopa = $this->db->get('formas_participacion')->first_row();

        //Contar la cantidad de participantes
        $this->db->where('obra_id', $obraId);
        $cantidad = $this->db->get('rel_obras_personas')->num_rows();

        //Comprobar si esta completo el cupo
        $data['estado'] = ($fopa->min_personas <= $cantidad && $cantidad <= $fopa->max_personas)?'completo':'incompleto';
        return $this->editar($obraId, $data);
    }
}
