<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tabla extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function obtenerPaises()
    {
        return $this->db->get('pais')->result();
    }

    public function obtenerProvincias()
    {
        return $this->db->get('estado')->result();
    }

}
