<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pais extends CI_Model
{
    private $tabla = array(
        'key' => 'id',
        'auto_key' => true,
        'tabla' => 'pais',
        'bool_delete' => false
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function obtener()
    {
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtenerXId($paisId)
    {
        $this->db->where('id', $paisId);
        return $this->db->get('pais')->first_row();
    }

    
    public function obtenerProvinciaXId($provId)
    {
        $this->db->where('id', $provId);
        return $this->db->get('estado')->first_row();
    }

    public function obtenerProvincias($paisId)
    {
        $this->db->where('pais_id', $paisId);
        return $this->db->get('estado')->result();
    }
    

}
