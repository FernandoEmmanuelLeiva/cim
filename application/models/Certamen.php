<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Certamen extends CI_Model
{

    private $tabla = array(
        'key' => 'cert_id',
        'auto_key' => false,
        'tabla' => 'certamenes',
        'bool_delete' => false  
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function setEstado($certId, $estado)
    {
        $data['estado'] = $estado;
        return $this->GModel->editar($certId, $data);
    }

    public function obtenerEstados()
    {
        $this->db->where('tabla', $this->tabla['tabla'].'_estados');
        $this->db->order_by('orden');
        return $this->db->get('utl_tablas')->result();
    }

    //Obras
    public function obtenerObras($certId)
    {   
        $this->db->select('T.*');
        $this->db->select('C.nombre as categoria');
        $this->db->select('FP.nombre as fopa, FP.precio');
        $this->db->select('I.nombre as nombre_instituto');
        $this->db->select('M.nombre as modalidad');
        $this->db->select('E.nombre as estilo');
        $this->db->join('formas_participacion FP', 'FP.fopa_id = T.fopa_id');
        $this->db->join('categorias C', 'C.cate_id = T.cate_id');
        $this->db->join('institutos I', 'I.inst_id = T.inst_id');
        $this->db->join('estilos E', 'E.esti_id = T.esti_id');
        $this->db->join('modalidades M', 'M.moda_id = E.moda_id');
        $this->db->order_by('nombre_instituto, nombre_obra');
        return $this->db->get('obras T')->result();
    }

    public function obtener()
    {
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtenerXId($certId)
    {
        return $this->GModel->obtener($certId, $this->tabla);
    }

    public function guardar($data)
    {
        log_message('DEBUG',json_encode($data));
        return $this->GModel->guardar($data, $this->tabla);
    }

    public function editar($certId, $data)
    {
        return $this->GModel->editar($certId, $data, $this->tabla);
    }

    public function eliminar($certId)
    {
        return $this->GModel->eliminar($certId, $this->tabla);
    }
    

}
