<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Persona extends CI_Model
{
    private $tabla = array(
        'key' => 'pers_id',
        'auto_key' => true,
        'tabla' => 'personas',
        'bool_delete' => false  
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function obtenerTodas()
    {
        return $this->GModel->obtenerTodas($this->tabla);
    }

    public function obtener($persId)
    {
        $this->load->model('Pais');
        $persona = $this->GModel->obtener($persId, $this->tabla);
        $persona->pais = $this->Pais->obtenerXId($persona->pais);
        $persona->provincia = $this->Pais->obtenerProvinciaXId($persona->provincia);
        return $persona;
    }
    
    public function obtenerXUserId($userId)
    {
        $this->db->where('user_id', $userId);
        $res =  $this->GModel->obtenerTodas($this->tabla);
        if(sizeof($res)){
            $user = reset($res);
            $this->load->model('Pais');
            $user->pais = $this->Pais->obtenerXId($user->pais);
            $user->provincia = $this->Pais->obtenerProvinciaXId($user->provincia);
            return $user;
        } 

    }

    public function obtenerXDni($dni)
    {
        $this->db->where('dni', $dni);
        return $this->db->get('personas')->first_row();
    }
    
    public function guardar($data)
    {
        return $this->GModel->guardar($data, $this->tabla);
    }

    public function editar($persId, $data)
    {
        return $this->GModel->editar($persId, $data, $this->tabla);
    }

    public function eliminar($persId)   
    {
        return $this->GModel->eliminar($persId, $this->tabla);
    }
}
