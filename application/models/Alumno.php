<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alumno extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Persona');
    }

    public function obtenerXInstituto($instId)
    {
        $this->db->select('T.*');
        $this->db->join('rel_personas_institutos PI', 'PI.pers_id = T.pers_id');
        $this->db->join('institutos I', 'I.inst_id = PI.inst_id');
        $this->db->where('PI.inst_id', $instId);
        $this->db->where('I.delete', false);
        return $this->Persona->obtenerTodas();
    }

    public function perteneceAInstituto($persId, $instId)
    {
        $this->db->where('inst_id', $instId);
        $this->db->where('pers_id', $persId);
        return $this->db->get('rel_personas_institutos')->num_rows();
    }

    public function asociarInstituto($persId, $instId)
    {
        $data = [
            'pers_id'=> $persId,
            'inst_id' => $instId
        ];
        return $this->db->insert('rel_personas_institutos', $data);
    }

    public function cantidadInstitutosAsociados($persId)
    {
        $this->db->where('pers_id', $persId);
        return $this->db->get('rel_personas_institutos')->num_rows();
    }

    public function eliminarDeInstituto($alumId, $instId)
    {
        $this->db->where('pers_id', $alumId);
        $this->db->where('inst_id', $instId);
        return $this->db->delete('rel_personas_institutos');
    }

    public function guardar($data)
    {
        return $this->Persona->guardar($data);
    }

    public function editar($alumId, $data)
    {
        return $this->Persona->editar($alumId, $data);
    }

    public function eliminar($alumId)   
    {
        return $this->Persona->eliminar($alumId);
    }

    public function obtenerXEdad($instId, $min, $max)
    {
        $this->db->where("$min <= T.edad and T.edad <= $max");
        return $this->obtenerXInstituto($instId);
    }
}
