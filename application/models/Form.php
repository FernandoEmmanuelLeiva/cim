<?php defined('BASEPATH') or exit('No direct script access allowed');

class Form extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function guardarValores($form, $infoId, $data)
    {
        $this->db->select("F.form_id, name, keywords, $infoId as info_id");
        $res = $this->obtenerXNombre($form);
        
        $this->db->insert_batch('frm_valores', $res);
        
        $xdata = [];
        foreach ($data as $key => $o) {
            $xdata[] = array(
                'name' => $key,
                'valor' => is_array($o)?str_replace('},{',',', implode(',',$o)):$o
            );
        }
        $this->db->where('info_id', $infoId);
        return $this->db->update_batch('frm_valores', $xdata, 'name');
    }

    public function eliminarValores($infoId)
    {
  
        $this->db->where('info_id', $infoId);
        return $this->db->delete('frm_valores');

    }

    public function obtenerXNombre($nombre, $emprId = false)
    {
        $this->db->from('frm_items as FI');
        $this->db->join('formularios as F', 'F.form_id = FI.form_id');
        #$this->db->join('rel_formularios_empresas as FE', 'FE.form_id = F.form_id');
        $this->db->where('F.nombre', $nombre);
        #$this->db->where('FE.empr_id', emp());
        $this->db->order_by('orden');
        $res = $this->db->get()->result();

        foreach ($res as $key => $o) {
            if(isset($o->tipo) && ($o->tipo == 'radio' || $o->tipo == 'checkbox')){
                $res[$key]->valores = $this->obtenerValores($o->valores, $emprId);
            }
        }

        return $res;
    }
    public function obtener($formId)
    {
        $this->db->from('frm_items as FI');
        $this->db->join('formularios as F', 'F.form_id = FI.form_id');
        #$this->db->join('rel_formularios_empresas as FE', 'FE.form_id = F.form_id');
        $this->db->where('F.form_id', $formId);
        #$this->db->where('FE.empr_id', emp());
        $this->db->order_by('orden');
        $res = $this->db->get()->result();

        foreach ($res as $key => $o) {
            if(isset($o->tipo) && ($o->tipo == 'radio' || $o->tipo == 'checkbox')){
                $res[$key]->valores = $this->obtenerValores($o->valores, $emprId);
            }
        }

        return $res;
    }

    public function obtenerValor($tablId)
    {
        $this->db->select('valor, descripcion');
        $this->db->where('tabl_id', $tablId);
        $this->db->where('delete', 0);
        return $this->db->get('tablas')->first_row();
    }

    public function obtenerValores($tabla , $emprId = false)
    {
        if($emprId) $this->db->where('empr_id', $emprId);
        $this->db->select('tabl_id, tabla, valor, descripcion');
        if(is_array($tabla)) $this->db->or_where_in('tabla', $tabla); else $this->db->where('tabla', $tabla);
        $this->db->where('delete', false);
        $this->db->order_by('tabla, descripcion');
        return $this->db->get('tablas')->result();
    }

    public function guardarValor($data, $emprId =  false)
    {
        if($emprId) $data['empr_id'] =  $emprId;
        return $this->db->insert('tablas', $data);
    }

    public function editarValor($id, $data)
    {
        $this->db->where('tabl_id', $id);
        return $this->db->update('tablas', $data);
    }

    public function eliminarValor($tablId)
    {
        $this->db->where('tabl_id', $tablId);
        return $this->db->delete('tablas');
    }

    public function datosGuardados($emprId, $key = false, $fi = false, $ff = false)
    {
        $this->db->select('FV.*, date(T.inicio) as fecha');
        $this->db->from('turnos T');
        $this->db->join('frm_valores FV', 'FV.info_id = T. turn_id');
        $this->db->where('empr_id', $emprId);
        $this->db->where('estado', 'CONFIRMADO');
        $this->db->where('valor is not null');
        $this->db->where('delete', 0);
        $this->db->order_by('T.inicio');
        if($fi) $this->db->where("'$fi' <= date(T.inicio)");
        if($ff) $this->db->where("date(T.fin) <= '$ff'");
        if($key) $this->db->where("lower(keywords) like '%$key%'");
        return $this->db->get()->result();
    }
}
