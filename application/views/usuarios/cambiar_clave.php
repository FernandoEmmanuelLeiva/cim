<style>
.msj {
    display: none
}
</style>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Restablecer Contraseña</p>
            <p id="msj" class="text-danger msj">Las contraseñas no coinciden</p>
            <form action="<?php echo base_url("usuarios/recuperarClave/$token") ?>" method="post">
                <div class="input-group mb-3">
                    <input id="pass" type="password" class="form-control" placeholder="Contraseña" name="password"
                        onkeyup="vPass()" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input id="rpass" type="password" class="form-control" placeholder="Repetir Contraseña"
                        onkeyup="vPass()" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-12">
                        <button id="registrar" type="submit" class="btn btn-admin float-right"><i
                                class="fas fa-check mr-1"></i>Guardar Cambios</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
<script>
function vPass() {
    if ($('#rpass').val() && $('#pass').val() != $('#rpass').val()) {
        $('#msj').show();
        $('#registrar').attr('disabled', true)
    } else {
        $('#msj').hide();
        $('#registrar').attr('disabled', false);
    }
}
</script>