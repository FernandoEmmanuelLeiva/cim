<table class="table table-striped table-hover">
    <thead>
        <th>Listado de usuarios</th>
        <th class="w-10">Estado</th>
        <th class="w-10 text-right"><button class="btn btn-admin btn-sm"><i class="fas fa-plus"></i></button></th>
    </thead>
    <tbody>
        <?php
        if($usuarios){
            foreach ($usuarios as $key => $o) {
                
                echo "<tr id='".enc($o->user_id)."'>";
                echo "<td class='lead'>$o->email</td>";
                echo "<td>".tag($o->estado, $o->estado=='ACTIVO'?'success':'')."</td>";
                echo "<td class='text-right'>".acciones('E|D')."</td>";
                echo "</tr>";
            }
        }else tablaVacia();
        ?>
    </tbody>
</table>