<div class="card">
    <div class="card-header">
        <h3 class="card-title">Usuarios del sistema</h3>
    </div>
    <div class="card-body">
        <?php 
            echo comp('pnl-usuarios', base_url('usuarios/obtener'), true);
        ?>
    </div>
</div>