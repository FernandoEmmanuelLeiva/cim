<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header') ?>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><?php echo TITULO_LOGIN ?></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Iniciar Sesión</p>

                <form action="<?php echo base_url('usuarios/login') ?>" method="post">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-admin float-right"><i
                                    class="fas fa-sign-out-alt mr-1"></i>Ingresar</button>
                        </div>
                        <div class="col-md-12">
                            <a href="<?php echo base_url('usuarios/recuperarClave') ?>">Olvide mi contraseña</a>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="social-auth-links text-center mb-3">
            <h4>¿Aún no estas registrado?</h4>

            <a href="<?php echo base_url('usuarios/registro') ?>" class="btn btn-block btn-admin registro">
                Registrate Ahora<i class="fas fa-exclamation ml-1"></i>
            </a>
            <a href="#" class="btn btn-block btn-danger" hidden>
                <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
            </a>
        </div>
        <!-- /.social-auth-links -->

        <p class="mb-1" hidden>
            <a href="#">Recuperar contraseña</a>
        </p>
        <p class="mb-0" hidden>
            <a href="#" class="text-center">Register a new membership</a>
        </p>

        <!-- /.login-card-body -->

    </div>
    <!-- /.login-box -->

    <?php $this->load->view('layout/scripts') ?>

</body>

</html>