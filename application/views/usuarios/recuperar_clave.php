<style>
.msj {
    display: none
}
</style>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Recuperar Contraseña</p>
            <?php 
                echo alert('Recuperar de Contraseña', 'Enviaremos un email a tu correo con un link para que puedas recuperar el acceso a tu cuenta', 'fas fa-info')
            ?>
            <form action="<?php echo base_url('usuarios/recuperarClave') ?>" method="post">
                <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Email" name="email" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-12">
                        <button id="registrar" type="submit" class="btn btn-admin float-right"><i
                                class="fas fa-check mr-1"></i>Recuperar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
           
        </div>
    </div>
</div>