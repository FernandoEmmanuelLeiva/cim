<!-- <div class="input-group input-group-smx"> -->
<select type="text" class="form-control" id="participante">
    <option selected disabled>- Selecccionar -</option>
    <?php
    foreach ($alumnos as $o) {
        echo "<option value='" . enc($o->pers_id) . "'>$o->dni - $o->nombre $o->apellido </option>";
    }
    ?>
</select>
<!-- <span class="input-group-append">
        <button type="button" class="btn btn-admin btn-flat"><i class="fas fa-plus"></i></button>
    </span>
</div> -->

<table class="table table-striped table-hover">
    <thead>
        <th>NyA</th>
        <th class="w-5">Edad</th>
        <th class="w-5"></th>
    </thead>
    <tbody>
        <?php
        if ($participantes) {
            foreach ($participantes as $o) {
                echo "<tr id='" . enc($o->pers_id) . "'>";
                echo "<td>$o->nombre $o->apellido <br><small class='badge badge-info'><b>DNI: </b>$o->dni</small></td>";
                echo "<td>" . calcularEdad($o->fecha_nacimiento) . "</td>";
                echo '<td><button class="btn btn-link" onclick="eliminarParticipante(this)"><i class="fa fa-times text-danger"></i></button></td>';
                echo "</tr>";
            }
        } else tablaVacia();
        ?>
    </tbody>
</table>

<script>
    $('#participante').on('change', function() {
        var persId = this.value;
        if ($(`tr#${persId}`).length > 0) {
            info('La persona ya participa en la obra');
            return;
        }
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: `<?php echo base_url('obras/participantes/') ?>${sObraId}`,
            data: {
                persId
            },
            success: function(res) {
                reload('#pnl-participantes', sObraId);
                hecho();
            },
            error: function(res) {
                ajaxError(res);
            },
            complete: function() {

            }
        })
    });

    function eliminarParticipante(e) {
        if (confirm('¿Confirma realizar esta acción?')) {
            $.ajax({
                type: 'GET',
                dataType: 'JSON',
                url: `<?php echo base_url('obras/eliminarParticipante/') ?>${sObraId}/${rowId(e)}`,
                success: function(res) {
                    remRow(e);
                    hecho();
                },
                error: function(res) {
                    ajaxError(res);
                },
                complete: function() {

                }
            })
        }
    }
</script>