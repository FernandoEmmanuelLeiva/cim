<?php echo modal('mdl-nuevo', 'Nueva obra', $this->load->view('obras/form', ['id'=>'frm-obra'], true),'<button class="btn btn-admin btn-nuevo"><i class="fas fa-check mr-1"></i>Guardar</button>'); ?>
<?php echo modal('mdl-editar', 'Editar obra', $this->load->view('obras/form', ['id'=>'frm-obra-e'], true),'<button class="btn btn-admin btn-editar"><i class="fas fa-check mr-1"></i>Guardar</button>'); ?>
<?php echo modal('mdl-agregar', 'Ver participantes', comp('pnl-participantes', base_url('obras/participantes'))); ?>

<table id="tbl-obras" class="table xtable-striped table-hover">
    <thead>
        <th>Listado de Obras <small>(Total obras: <?php echo sizeof($obras)?>)</small></th>
        <th class="w-10">Estado</th>
        <th class="w-10 text-right">
            <?php if(checkRol('USUARIO')){ ?>
            <button class="btn btn-sm btn-admin btn-nuevo"><i class="fas fa-plus"></i></button>
            <?php } ?>
        </th>
    </thead>
    <tbody class="text-up">
        <?php 
            if($obras){
                $inst = '';
                foreach ($obras as $o) {   

                    if(isset($o->nombre_instituto) && strcmp($inst, $o->nombre_instituto) != 0){
                        $inst = $o->nombre_instituto;
                        echo "<tr class='bg-gray-light'><td colspan='4' class='font-bold text-center'>$o->nombre_instituto</td></tr>";
                    }

                    echo "<tr id='".enc($o->obra_id)."'>";
                    echo "<td><h2 class='lead'>$o->nombre_obra</h2>";
                    echo "<cite class='text-gray'>$o->modalidad | $o->estilo</cite><br>";
                    echo "<small class='badge badge-secondary mr-1'>$o->fopa | $o->categoria</small>";
                    echo "<small class='badge badge-secondary mr-1'><i class='far fa-clock mr-1'></i>$o->duracion</small>";
                    echo "<small class='badge badge-admin mr-1 subtotal'>Subtotal: <i class='fas fa-dollar-sign mr-1'></i><subtotal>$o->precio</subtotal></small>";
                    echo "</td>";
                    echo "<td>".estado($o->estado)."</td>";
                    echo "<td class='text-right'>".acciones('E|D', ['btn-agregar' => 'Ver Participantes'])."</td>";
                    echo "</tr>";
                }
            }else tablaVacia();
        ?>
    </tbody>
</table>

<script>
$('#mdl-editar .modal-body').prepend(
    `<?php echo alert('Importante', 'Al cambiar la Categoria y/o Forma Participación se deberán asignar nuevamente los participantes a la obra', 'fas fa-exclamation', 'info') ?>`
    );
onModalClose('#mdl-agregar', function() {
    reload('#pnl-obras');
})
var sObraId;
$('.btn-nuevo').click(function() {
    if (modalOpen()) {
        frmSubmit('#frm-obra', false, reload, '#pnl-obras');
    } else {
        $('#mdl-nuevo').modal('show');
    }

})

$('.btn-editar').click(function() {
    if (modalOpen()) {
        frmSubmit('#frm-obra-e', sObraId, reload, '#pnl-obras');
    } else {
        sObraId = rowId(this);
        frmFillFromUrl(`<?php echo base_url('obras/obtener/') ?>${sObraId}`, '#frm-obra-e');
        $('#mdl-editar').modal('show');
    }
});

$('.btn-agregar').click(function() {
    if (modalOpen()) {

    } else {
        sObraId = rowId(this);
        $('#mdl-agregar').modal('show');
        reload('#pnl-participantes', sObraId);
    }
});

$('.btn-eliminar').click(function() {
    var e = this;
    if (confirm('Desea eliminar la obra?')) {
        $.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: `<?php echo base_url('obras/eliminar/') ?>${rowId(e)}`,
            success: function(res) {
                if(res.status){
                    remRow(e);
                    hecho();
                }else{
                    falla();
                }
            },
            error: function(res) {
                error();
            },
            complete: function() {

            }
        })
    }
})

reporte();

function reporte() {
    $('#cantidad_obras').html($('#tbl-obras > tbody > tr').length);
    $('#completos').html($('.estado-completo').length)
    $('#incompletos').html($('.estado-incompleto').length);
    $('#total').html('$' + sumarValoresHtml('subtotal'))
}
</script>