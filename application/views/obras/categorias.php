<option value="PRE-BABY" data-min="2" data-max="3">PRE-BABY (2 a 3 años)</option>
<option value="BABY" data-min="4" data-max="5">BABY (4 a 5 años)</option>
<option value="PRE-INFANTIL" data-min="6" data-max="7">PRE-INFANTIL (6 a 7 años)</option>
<option value="INFANTIL1" data-min="8" data-max="9">INFANTIL I (8 a 9 años)</option>
<option value="INFANTIL2" data-min="10" data-max="11">INFANTIL II (10 a 11 años)</option>
<option value="JUVENIL1" data-min="12" data-max="13">JUVENIL I (12 a 13 años)</option>
<option value="JUVENIL2" data-min="14" data-max="15">JUVENIL II (14 a 15 años)</option>
<option value="JUVENIL3" data-min="16" data-max="17">JUVENIL III (16 a 17 años)</option>
<option value="MAYORES1" data-min="18" data-max="21">MAYORES I (18 a 21 años)</option>
<option value="MAYORES2" data-min="22" data-max="30">MAYORES II (22 a 30 años)</option>
<option value="ADULTOS" data-min="30" data-max="-">ADULTOS (Mas de 30 años)</option>
<option value="PROFESIONAL1" data-min="20" data-max="24">PROFESIONAL I (20 a 24 años)</option>
<option value="PROFESIONAL2" data-min="25" data-max="30">PROFESIONAL II (25 a 30 años)</option>
<option value="PROFESIONAL3" data-min="31" data-max="40">PROFESIONAL III (31 a 40 años)</option>
<option value="PROFESIONAL4" data-min="41" data-max="100">PROFESIONAL IV (Mas de 41 años)</option>