<?php if(dev()){ ?>
<div class="col-12">
    <small><a href="#" onclick="showHide('#header_info')">Mostrar/Ocultar Información</a></small>
</div>
<?php } ?>
<div id="header_info" class="row">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="far fa-star"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total obras</span>
                <span id="cantidad_obras" class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-success"><i class="far fa-check-circle"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Completas</span>
                <span id="completos" class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-warning"><i class="fas fa-exclamation-triangle"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Incompletas</span>
                <span id="incompletos" class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-dollar-sign"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><?php echo checkRol('USUARIO')?'Total a Pagar':'Total a Cobrar' ?></span>
                <span id="total" class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<script>
  if(!window.mobileCheck) showHide('#header_info', true);
</script>