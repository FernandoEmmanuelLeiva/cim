<form id="<?php echo $id ?>" action="<?php 
    if(isset($certId) && isset($instId))
    {
        echo base_url('obras/registrar/'.enc($certId)."/".enc($instId)); 
    } 
    ?>">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Nombre<strong class="text-danger"> *</strong>:</label>
                <input class="form-control" required value="" type="text" placeholder="" id="nombre_obra" name="nombre_obra">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Maestro Preparador<strong class="text-danger"> *</strong>:</label>
                <select name="dni_maestro_preparador" class="form-control" required>
                    <option selected disabled value>- Seleccionar -</option>
                    <?php
                        foreach ($maestros_preparadores as $o) {
                            echo "<option value='$o->dni'>$o->nombre $o->apellido</option>";
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Nombre canción<strong class="text-danger"> *</strong>:</label>
                <input class="form-control" required value="" type="text" placeholder="" id="nombre_cancion" name="nombre_cancion">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Duración<strong class="text-danger"> *</strong> (min:seg):</label>
                <input class="form-control" required value="" type="text" placeholder="" id="duracion" name="duracion">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Modalidad:</label>
                <select id="moda_id" name="moda_id" class="form-control" required>
                    <?php
                  
                        echo selectOpts($modalidades, 'moda_id', 'nombre');
                  
                   ?>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Estilo:</label>
                <select id="esti_id" name="esti_id" class="form-control" required>
                    <?php
                 
                        echo selectOpts($estilos, 'esti_id', 'nombre', 'moda_id', 'modalidad');
                   
                  ?>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Categoría<strong class="text-danger"> *</strong>:</label>
                <select class="form-control frm-select" id="cate_id" name="cate_id" width="100%" required>
                    <option selected disabled value> - Seleccionar - </option>
                    <?php
                        foreach ($categorias as $o) {
                            if($o->min_edad == $o->max_edad){
                                echo "<option value='".enc($o->cate_id)."' data-json='".(json_encode($o))."'>$o->nombre (<b>$o->min_edad</b> años)</option>";
                            }
                            else{
                                echo "<option value='".enc($o->cate_id)."' data-json='".(json_encode($o))."'>$o->nombre (De <b>$o->min_edad</b> a <b>$o->max_edad</b> años)</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="">Formar de Participación<strong class="text-danger"> *</strong>:</label>
                <select class="form-control frm-select" id="fopa_id" name="fopa_id" width="100%" required>
                    <option selected disabled value> - Seleccionar - </option>
                    <?php
                        foreach ($formas_participacion as $o) {
                            if($o->min_personas == $o->max_personas){
                                echo "<option value='".enc($o->fopa_id)."' data-json='".(json_encode($o))."'>$o->nombre (<b>$o->min_personas</b> personas)</option>";
                            }else{
                                echo "<option value='".enc($o->fopa_id)."' data-json='".(json_encode($o))."'>$o->nombre (De <b>$o->min_personas</b> a <b>$o->max_personas</b> personas)</option>";
                            }
                        }
                    ?>
                </select>
            </div>
        </div>

    </div>
</form>


<script>
$('#<?php echo $id ?>').find('#duracion').inputmask('99:99');
resetSelects()

$('#<?php echo $id ?>').find('#moda_id').on('change', function() {
    resetSelects();
    $('#<?php echo $id ?>').find('#esti_id').prop('disabled', false);
    $('#<?php echo $id ?>').find(`.modalidad${this.value}`).show();
});

function resetSelects() {
    $('#<?php echo $id ?>').find('#esti_id').find('option').hide();
    $('#<?php echo $id ?>').find('#esti_id').prop('disabled', true);
    $('#<?php echo $id ?>').find('#esti_id').val($('#<?php echo $id ?>').find('#esti_id').find("option:first").val());
}
</script>