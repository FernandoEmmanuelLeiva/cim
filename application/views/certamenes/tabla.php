<style>
.btn-estado {
    cursor: pointer;
}

.estado-habilitado {
    color: white;
    background-color: #28a745;
}

.estado-deshabilitado {
    color: white;
    background-color: #ffc107;
}

.estado-finalizado {
    color: white;
    background-color: #ff851b;
}
</style>

<?php
    echo modal('mdl-nuevo', 'Nuevo certamen', getForm('frm-certamen', 'CERTAMEN'), '<button class="btn btn-admin btn-nuevo"><i class="fas fa-check mr-1"></i>Guardar</button>');
    echo modal('mdl-editar', 'Editar certamen', getForm('frm-certamen-e','CERTAMEN'), '<button class="btn btn-admin btn-editar"><i class="fas fa-check mr-1"></i>Guardar</button>');
    echo modal('mdl-estado', 'Estado certamen', $this->load->view('certamenes/tabla_estados', false , true) , '', true);
?>

<table class="table table-striped table-hover">
    <thead>
        <th>Listado de cetámenes</th>
        <th class="w-10">Estado</th>
        <th class="text-right w-10"><button class="btn btn-admin btn-sm" onclick="$('#mdl-nuevo').modal('show');"><i
                    class="fas fa-plus"></i></button></th>
    </thead>
    <tbody>
        <?php
        if($certamenes){

            foreach ($certamenes as $key => $o) {
                
                echo "<tr id='".enc($o->cert_id)."'>";
                echo  "<td><h2 class='lead'>$o->nombre</h2>";
                echo  "<small><cite>".(strlen($o->descripcion)==0?'Sin descripción':$o->descripcion)."</cite></small><br>";
                echo  tag("<b>Desde:</b> " .fecha($o->fecha_inicio)).tag("<b>Hasta:</b> " .fecha($o->fecha_fin));
                #echo  "<small><a href='#'>Mas detalles</a></small>";
                echo  "</td>";
                echo  "<td><small class='badge btn-estado text-right estado-$o->estado'>".strtoupper($o->estado)."</small></td>";
                echo  "<td class='text-right'>".acciones('E|D')."</td>";
                echo '</tr>';
                
            }
        }else tablaVacia();
        ?>
    </tbody>
</table>

<script>
var srowId
$('.btn-nuevo').click(function() {
    wb(this);
    frmSubmit('#frm-certamen', false, reload, '#tbl-certs');
});

$('.btn-editar').click(function() {
    if (modalOpen()) {
        wb(this);
        frmSubmit('#frm-certamen-e', srowId, reload, '#tbl-certs');
    } else {
        srowId = rowId(this);
        frmFillFromUrl(`<?php echo base_url('certamenes/obtenerXId/') ?>${srowId}`, '#frm-certamen-e');
        $('#mdl-editar').modal('show');
    }
})

$('.btn-eliminar').click(function() {
    var e = this;
    if (!confirm('¿Desea eliminar el elemento?')) return;
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: `<?php echo base_url('certamenes/eliminar/') ?>${rowId(e)}`,
        success: function(res) {
            if (res.status) {
                hecho();
                remRow(e);
            } else error();
        },
        error: function(res) {
            error();
        }
    })
})

$('.btn-estado').click(function() {
    if (modalOpen()) {
        var estado = rowId(this);
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: `<?php echo base_url('certamenes/estado/') ?>${srowId}`,
            data: {
                estado
            },
            success: function(res) {
                if (res.status) {
                    hecho();
                    $('#mdl-estado').modal('hide');
                    reload('#tbl-certs');
                } else error();
            },
            error: function(res) {
                error();
            },
            complete: function() {

            }
        })
    } else {
        srowId = rowId(this);
        $('#mdl-estado').modal('show');
    }
})
</script>