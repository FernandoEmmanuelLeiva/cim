<div class="card card-gray">
    <div class="card-header">
        <h3 class="card-title">Certamenes</h3>
    </div>
    <div class="<?php dev()?'':'card-body' ?>">
        <?php echo comp('tbl-certs', base_url('certamenes/lista'), true)?>
    </div>
</div>