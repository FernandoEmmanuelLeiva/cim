<div class="row">
    <div class="col-md-6">
      <?php echo comp('pnl-info', base_url('institutos/obtenerInfo'), enc($instId)) ?>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-orange">
                    <div class="card-header">
                        <h5 class="card-title"><i class="fas fa-users mr-2"></i>Carga de Participantes</h5>
                    </div>
                    <div class="card-body">
                        <h5 class="mb-3"><b>Registro de Alumnos Participantes / Maestros Preparadores</b></h5>
                        <p class="card-text"><cite>Previamente al Registro de Obras se deberá registrar todos los
                                Participantes del Certamen / Maestro Preparadores.</cite></p>
                        <hr>
                        <a href="#" class="btn btn-admin" onclick="linkTo('alumnos/index/<?php echo enc($instId) ?>')"><i class="fa  fa-user-plus"></i>
                            Carga
                            Participates</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-outline card-orange">
                    <div class="card-header">
                        <h5 class="card-title"><i class="fas fa-book mr-2"></i>Carga de Obras</h5>
                    </div>
                    <div class="card-body">
                        <h5><b>Registro de Obras</b></h5>
                        <p class="card-text mb-2"><cite>Posteriormente al Registro de Alumnos Participantes se
                                Registrarán
                                las
                                obras que el
                                instituto llevará a cabo en el certamen.</cite></p>
                        <hr>
                        <a href="#" class="btn btn-admin" onclick="linkTo('obras/index/<?php echo enc($instId)?>')"><i class="fas fa-plus"></i> Ingresar
                            Obras</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>