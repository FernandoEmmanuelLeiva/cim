<div class="card">
    <div class="card-header">
        <?php echo selectData('cert_id', 'cert_id', 'nombre', $certamenes) ?>
    </div>
    <div class="card-body">
        <?php echo comp('pnl-institutos', base_url('institutos/obtenerReporte')) ?>
    </div>
</div>

<script>
$('select#cert_id').on('change',function() {
    reload('#pnl-institutos', this.value);
})
</script>