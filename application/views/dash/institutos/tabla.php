<?php echo modal('mdlMoreInfo', 'Información institución', comp('pnl-more-indo', base_url('obras/participantes'))); ?>


<table class="table table-hover">
    <thead>
        <th>
            Nombre Institución
        </th>
        <th class="w-10 text-right">Acciones</th>
    </thead>
    <tbody>
        <?php 

            if($institutos){

                foreach ($institutos as $o) {
                    
                    echo "<tr>";
                    echo "<td>";
                    
                    echo "<text class='lead'>$o->nombre</text><br>";
                    echo tag($o->email);
                    echo tag(ic('phone')."$o->telefono");
                    echo tag("Habilitado", 'success');
                    echo "</td>";
                    echo "<td class='text-right'>".acciones('D', ['more_info' => 'Ver información'])."</td>";
                    echo "</tr>";

                }

            }else tablaVacia();

        ?>
    </tbody>
</table>