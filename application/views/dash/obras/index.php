<?php $this->load->view('obras/header_info'); ?>
<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">Obras</h3>
    </div>
    <div class="<?php echo dev()?'':'card-body'?>">
    <?php
        $certId = enc(certId());
        echo comp('pnl-obras', base_url("index.php/certamenes/".enc(1)."/obras"), true);
    ?>
    </div>
</div>