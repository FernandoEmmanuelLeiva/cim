<?php
    $this->load->view('alumnos/mdl_nuevo');
    if (HABILITAR_CAMBIOS) $this->load->view('alumnos/mdl_editar');
?>


<div class="card card-outline card-orange">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-users"></i> Listado de Participantes</h3>
    </div>
    <div class="<?php echo dev() ? '' : 'card-body' ?>">
        <button class="btn btn-admin btn-block" onclick="$('#nuevo').modal('show')"> <i class="fa fa-user-plus"></i> Nuevo
            Participante</button>
        <?php

        echo comp('tbl_alumnos', base_url("alumnos/obtenerXInstituto/" . enc($instId)), true)

        ?>
    </div>
</div>