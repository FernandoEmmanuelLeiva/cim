<script>

function editar(button) {
    var data = getForm($('#frm-alumno-e'));
    data.maestro_preparador = data.maestro_preparador ? 1 : 0;
    if (!validar(data)) return;

    wb(button);
    $.ajax({
        type: 'POST',
        dataType:'JSON',
        url: '<?php echo base_url() ?>alumnos/editar',
        data,
        success: function(res) {
            if(res.status){
                reload('#tbl_alumnos');
                $('#editar').modal('hide');
                $('#frm-alumno-e')[0].reset();
                hecho();
            }else error();
        },
        error: function(result) {
            error();
        },
        complete: function() {
            wb(button);
        }
    });
}

// function validar(data) {
//     if (!bvValidarForm('frm-alumno-e')) return false;

//     if (data.dni.length != 8) {
//         alert('El DNI Ingresado es Inválido - Solo debe tener 8 dígitos sin puntos y sin guiones');
//         return false;
//     }

//     if ($('#' + data.dni).length != 0) {
//         alert('El DNI Ingresado ya fue Registrado');
//         return false;
//     }

//     return true;
// }

</script>

<!-- The Modal -->
<div class="modal" id="mdl-editar">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Editar Participante</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form id="frm-alumno-e">
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input id="nombre" onkeyup="" name="nombre" class="form-control text-up" type="text"
                            data-bv-notempty="" data-bv-notempty-message="Campo Obligatorio *">
                    </div>
                    <div class="form-group">
                        <label>Apellido:</label>
                        <input id="apellido" name="apellido" class="form-control text-up" type="text"
                            data-bv-notempty="" data-bv-notempty-message="Campo Obligatorio *">
                    </div>
                    <div class="form-group">
                        <label>DNI:</label>
                        <input id="dni" name="dni" class="form-control" type="number" data-bv-notempty=""
                            data-bv-notempty-message="Campo Obligatorio *">
                    </div>
                    <div class="form-group">
                        <label>Edad:</label>
                        <input id="edad" name="edad" class="form-control" type="number" data-bv-notempty=""
                            data-bv-notempty-message="Campo Obligatorio *">
                    </div>
                    <div class="form-group icheck-primary ml-n4">
                        <label><input name="maestro_preparador" value="1" class="form-control" type="checkbox"> Maestro
                            Preparador</label>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-admin btn-accion" onclick="editar(this)"><i
                        class="fa fa-check mr-1"></i>Guardar</button>
            </div>

        </div>
    </div>
</div>