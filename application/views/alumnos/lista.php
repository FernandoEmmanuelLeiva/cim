<table class="table table-striped" id="list">
    <thead>
        <th width="5%">N°</th>
        <th>Apellido y Nombre</th>
        <th width="15%">DNI</th>
        <th width="1%" class="acciones text-center"></th>
    </thead>
    <tbody>
        <?php

        if ($alumnos) {
            foreach ($alumnos as $key => $o) {
                echo "<tr id='" . enc($o->pers_id) . "' class='json' data-json='" . json_encode($o) . "'>";
                echo "<td class='text-bold'>" . ($key + 1) . "</td>";
                echo "<td>$o->apellido $o->nombre<br>" . ($o->maestro_preparador ? tag('Maestro Preparador') : '') . tag("Edad: $o->edad", 'warning') . "</td>";
                echo "<td>$o->dni</td>";

                echo "<td class='text-center acciones'>" . (HABILITAR_CAMBIOS ? acciones('E|D') : false) . "</td>";
                echo "</tr>";
            }
        }
        ?>
    </tbody>
</table>

<script>
    var srowId = false;
    $('.btn-editar').click(function(e) {
        if (modalOpen()) {
            wb(e);
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: `<?php echo base_url() ?>${rowId(this)}`,
                success: function(res) {
                    hecho();
                },
                error: function(res) {
                    error();
                },
                complete: function() {
                    wb(e);
                }
            })
        } else {
            srowId = rowId(this);
            fillForm(getJson(this), '#frm-alumno-e');
            $('#mdl-editar').modal('show');
            console.log(srowId);
        }
    })

    DataTable('#list', [0, 3]);

    $('.btn-eliminar').click(function(e) {

        if (!confirm('¿Desea Eliminar el Participante?')) return;

        $.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: `<?php echo base_url() ?>/alumnos/eliminar/${rowId(this)}/<?php echo enc($instId)?>`,
            success: function(res) {
                if (res.status) {
                    $(e).closest('tr').remove();
                    reload('#tbl_alumnos');
                    n_hecho();
                } else error();
            },
            error: function(res) {
                error();
            }
        });
    });

    function editar(btn) {
        wb(btn);
        var data = getForm('#frm-alumno-e');
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: `<?php echo base_url() ?>alumnos/editar/${srowId}`,
            data,
            success: function(res) {
                if (res.status) {
                    $('#mdl-editar').modal('hide');
                    hecho();
                    reload('#tbl_alumnos')
                } else {
                    error();
                }
            },
            error: function(res) {
                error();
            },
            complete: function() {
                wb(btn);
            }
        })
    }
</script>