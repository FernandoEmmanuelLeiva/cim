<style>
  .btn-admin {
    background: #ff6701;
    color: #ffffff;
  }

  .registro {
    font-size: 20px;
  }

  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* input {
    text-transform: uppercase;
  } */

  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }

  .nav-link {
    color: #ffffff !important;
  }

  .w-10 {
    width: 10%;
  }

  .w-5 {
    width: 10%;
  }

  .text-gray {
    color: gray
  }

  .text-up {
    text-transform: uppercase;
  }

  .badge-admin {

    color: #fff;
    background-color: #fd7e14;
  }
</style>