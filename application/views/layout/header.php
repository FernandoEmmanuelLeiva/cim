<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE ?></title>

    <link rel="icon" type="image/png" href="favicon.png"/>

    <!-- <link rel="manifest" href="./manifest.json" /> -->
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
        href="<?php echo base_url(LIB) ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/icheck/skins/all.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/jqvmap/jqvmap.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/daterangepicker/daterangepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/summernote/summernote-bs4.css">
    <!-- Validador -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/bootstrapvalidator/bootstrapValidator.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <link href="<?php echo base_url(LIB) ?>plugins/bootstrap-toggle/css/bootstrap-toggle.css" rel="stylesheet">

    <!-- SWAL ALERT -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>swal/dist/sweetalert2.min.css">

    <!-- SELECT2 -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins\select2-bootstrap4-theme\select2-bootstrap4.css">
    <link rel="stylesheet"
        href="<?php echo base_url(LIB) ?>plugins\bootstrap-switch\css\bootstrap4\bootstrap-switch.min.css">

    <?php $this->load->view('layout/mycss'); ?>


    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>fullcalendar\main.min.css">
    <script src="<?php echo base_url(LIB) ?>fullcalendar\main.min.js"></script>
    <script src="<?php echo base_url(LIB) ?>fullcalendar\locales-all.min.js"></script>

        

    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>datepicker\css\bootstrap-datepicker3.standalone.min.css">
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>timepicker\jquery.timepicker.min.css">

    <!-- jQuery -->
    <script src="<?php echo base_url(LIB) ?>plugins/jquery/jquery.min.js"></script>
</head>