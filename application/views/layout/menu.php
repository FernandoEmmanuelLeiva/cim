  <!-- Sidebar Menu -->
  <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        
          <li class="nav-item">
              <a href="#" class="nav-link" onclick="linkTo('admin/index/true')">
                  <i class="nav-icon fas fa-home"></i>
                  <p>
                      Principal
                  </p>
              </a>
          </li>  
          <li class="nav-header">SISTEMA</li>
          <li class="nav-item">
              <a href="#" class="nav-link " onclick="linkTo('certamenes')">
                  <i class="nav-icon fas fa-check"></i>
                  <p>
                      Certamenes
                  </p>
              </a>
          </li>
          <li class="nav-item">
              <a href="#" class="nav-link" onclick="linkTo('usuarios')">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                      Usuarios
                  </p>
              </a>
          </li>
          <li class="nav-header">REPORTES</li>
          <li class="nav-item">
              <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-dollar-sign"></i>
                  <p>
                      Recaudación
                  </p>
              </a>
          </li>
          <li class="nav-item">
              <a href="#" class="nav-link" onclick="linkTo('institutos/index')">
                  <i class="nav-icon far fa-building"></i>
                  <p>
                      Institutos
                  </p>
              </a>
          </li>
          <li class="nav-item">
              <a href="#" class="nav-link" onclick="linkTo('certamenes/obras')">
                  <i class="nav-icon far fa-star"></i>
                  <p>
                      Obras
                  </p>
              </a>
          </li>
          <li class="nav-item">
              <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>
                      Personas
                  </p>
              </a>
          </li>
      </ul>
  </nav>
  <!-- /.sidebar-menu -->