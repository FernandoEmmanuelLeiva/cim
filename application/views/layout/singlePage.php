<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="<?php echo base_url("favicon.png") ?>" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Validador -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/bootstrapvalidator/bootstrapValidator.css">

    <!-- SWAL ALERT -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>swal/dist/sweetalert2.min.css">

    <!-- SELECT2 -->
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins\select2-bootstrap4-theme\select2-bootstrap4.css">
    <link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins\icheck\skins\all.css">

    <?php $this->load->view('layout/mycss') ?>

    <!-- jQuery -->
    <script src="<?php echo base_url(LIB) ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(LIB) ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?php echo base_url(LIB) ?>plugins/bootstrapvalidator/bootstrapValidator.min.js"></script>

    <script src="<?php echo base_url(LIB) ?>props\forms.js"></script>

    <script src="<?php echo base_url(LIB) ?>props\navegacion.js"></script>

    <script src="<?php echo base_url(LIB) ?>props/selects.js"></script>

    <script src="<?php echo base_url(LIB) ?>swal/dist/sweetalert2.min.js"></script>

    <script src="<?php echo base_url(LIB) ?>props\notificaciones.js"></script>

    <script src="<?php echo base_url(LIB) ?>props\jquery.inputmask.min.js"></script>

    <script src="<?php echo base_url(LIB) ?>plugins\select2\js\select2.full.min.js"></script>

    <script src="<?php echo base_url(LIB) ?>plugins\icheck\icheck.min.js"></script>

</head>

<body class="hold-transition login-page gradiente layout-navbar-fixed sidebar-collapse">
    <div class="wrapper" id=content>
        <?php if (isset($view)) $this->load->view($view);
        else echo $html; ?>
    </div>

    <script>
        var baseUrl = '<?php echo baseurl() ?>';
    </script>
</body>

</html>