<div class="login-box text-center">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <i class="<?php echo $iconClass ?> fa-5x mb-3"></i>
            <div class="login-logo">
                <h3><?php echo $titulo ?></h3>
            </div>
            <?php
                if(isset($msj)) echo " <text class='lead'>$msj</text>";
         
                if(isset($btn) && $btn) echo " <hr><a href='$btnLink' class='btn btn-admin'>$btn</a>";
            ?>
        </div>
        <!-- /.login-logo -->
    </div>
</div>
<!-- /.login-box -->