<!-- jQuery -->
<!-- <script src="<?php #echo base_url(LIB) ?>plugins/jquery/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(LIB) ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(LIB) ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<!-- <script src="<?php echo base_url(LIB) ?>plugins/chart.js/Chart.min.js"></script> -->
<!-- Sparkline -->
<!-- <script src="<?php echo base_url(LIB) ?>plugins/sparklines/sparkline.js"></script> -->
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(LIB) ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<!-- <script src="<?php echo base_url(LIB) ?>plugins/jqvmap/jquery.vmap.min.js"></script> -->
<!-- <script src="<?php echo base_url(LIB) ?>plugins/jqvmap/maps/jquery.vmap.world.js"></script> -->
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(LIB) ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(LIB) ?>plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(LIB) ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(LIB) ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url(LIB) ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(LIB) ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(LIB) ?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php #echo base_url(LIB) ?>dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(LIB) ?>props/navegacion.js"></script>
<script src="<?php echo base_url(LIB) ?>props/settings.js"></script>

<!-- Formularios Genericos -->
<div id="frm-list" data-frmurl='<?php #echo FRM ?>'></div>
<script src="<?php echo base_url(LIB) ?>props/forms.js"></script>
<script src="<?php echo base_url(LIB) ?>plugins/bootstrapvalidator/bootstrapValidator.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(LIB) ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(LIB) ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src="<?php echo base_url(LIB) ?>plugins/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url(LIB) ?>plugins/icheck/icheck.min.js"></script>
<!-- <script src="<?php #echo base_url(LIB) ?>plugins/select2/js/select2.min.js"></script> -->
<script src="<?php echo base_url(LIB) ?>plugins\select2\js\select2.full.min.js"></script>
<!-- SWAL ALERT -->
<script src="<?php echo base_url(LIB) ?>swal/dist/sweetalert2.min.js"></script>
<script src="<?php echo base_url(LIB) ?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

<script src="<?php echo base_url(LIB) ?>timepicker\jquery.timepicker.min.js"></script>
<script src="<?php echo base_url(LIB) ?>datepicker\js\bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(LIB) ?>datepicker\locales\bootstrap-datepicker.es.min.js"></script>

<script src="<?php echo base_url(LIB) ?>props/tabla.js"></script>
<script src="<?php echo base_url(LIB) ?>props/forms.js"></script>
<script src="<?php echo base_url(LIB) ?>props/selects.js"></script>
<script src="<?php echo base_url(LIB) ?>props/notificaciones.js"></script>
<script src="<?php echo base_url(LIB) ?>props/settings.js"></script>
<script src="<?php echo base_url(LIB) ?>props/date.format.js"></script>
<script src="<?php echo base_url(LIB) ?>props\countdown.js"></script>
<script src="<?php echo base_url(LIB) ?>props\jquery.inputmask.min.js"></script>
<script>
var baseUrl = '<?php echo baseurl() ?>';
countdown.setLabels(
    ' milissegundo| segundo| minuto| hora| dia| semana| mês| ano| década| século| milênio',
    ' milissegundos| segundos| minutos| horas| dias| semanas| meses| anos| décadas| séculos| milênios',
    ' e ',
    ' + ',
    'agora');

function rowId(e) {
    return $(e).closest('tr').attr('id');
}

function remRow(e) {
    $(e).closest('tr').remove();
}

function modalOpen() {
    return $('.modal:visible').length && $('body').hasClass('modal-open');
}

function onModalClose(id, f) {
    $(id).on('hidden.bs.modal', function() {
        f();
    })
}

function sumarValoresHtml(e) {
    var acum = 0;
    var valor;
    $(e).each(function() {
        valor = $(this).html();
        acum += parseInt((valor == null || valor == undefined || valor == "") ? 0 : valor);
    })
    return acum;
}

function showHide(e, refresh = false) {
    var ban;
    if(refresh){
        ban = window.localStorage.getItem(e) != 'show';
    }else{
        ban = $(e).is(":visible");
    }

    if (ban) {
        $(e).hide();
        window.localStorage.setItem(e, 'hide');
    } else {
        $(e).show()
        window.localStorage.setItem(e, 'show');
    }
}
</script>