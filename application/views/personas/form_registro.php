<div class="login-box mt-0 mb-0">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <h5 class="text-center">Completar datos personales</h5>
            <?php
            echo getForm('frm-persona', 'DATOS PERSONALES');
            ?>
        </div>
    </div>
</div>

<script>
    $('#guardar').attr('disabled', true);
    $('#dni').keyup(function() {
        $('.frm-msj').html((this.value.length != 8) ? "El DNI debe tener 8 dígitos" : "");
        $('#guardar').attr('disabled', (this.value.length != 8));
    })

    $('select').select2({
        theme: 'bootstrap4'
    })

    fillSelectOptions('#pais', '<?php echo base_url() ?>paises/options');
    $('#pais').change(function() {
        fillSelectOptions('#provincia', `<?php echo base_url() ?>paises/${this.value}/estados/options`);
    })
</script>