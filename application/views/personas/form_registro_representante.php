<div class="login-box mt-0 mb-0">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-header">
            <form>
                <div class="form-group">
                    <label>¿Eres el representante legal de esta intitución?</label>
                    <label for=""><input type="radio">Si</label>
                    <label for=""><input type="radio">No</label>
                </div>
            </form>
        </div>
        <div class="card-body login-card-body">
            <h5 class="text-center">Completar datos personales</h5>
            <?php
            echo getForm('frm-persona', 'DATOS PERSONALES');
            ?>
        </div>
    </div>
</div>

<script>
    $('select').select2({
        theme: 'bootstrap4'
    })

    fillSelectOptions('#pais', '<?php echo base_url() ?>paises/options');
    $('#pais').change(function() {
        fillSelectOptions('#provincia', `<?php echo base_url() ?>paises/${this.value}/estados/options`);
    })
</script>