<?php if ($instituto->type == 'INSTITUTO') { ?>

    <div class="card card-outline card-orange">
        <div class="card-header">
            <h5 class="card-title"><i class="fas fa-building mr-2"></i>Información Institución</h5>
        </div>

        <table class="table table-hover table-striped text-up">
            <tbody>
                <tr>
                    <td><b>Nombre:</b><br><?php echo $instituto->nombre ?></td>
                </tr>

                <tr>
                    <td><b>Contacto:</b><br><?php echo $instituto->email ?> | <?php echo $instituto->telefono ?></td>
                </tr>

                <tr>
                    <td><b>Dirección:</b><br><?php echo $instituto->direccion ?> - <?php echo $instituto->provincia->nombre ?> - <?php echo $instituto->pais->nombre ?></td>
                </tr>

            </tbody>
        </table>
        <div class="card-footer">
            <a href="#" class="card-link btn-editar"><i class="fas fa-edit mr-1"></i>Editar Información</a>
        </div>
    </div>

<?php } ?>

<div class="card card-outline card-orange">
    <div class="card-header">
        <h5 class="card-title"><i class="fas fa-user mr-2"></i>
            <?php echo ($instituto->type == 'INSTITUTO') ? 'Director de Institución' : 'Datos Participante' ?>
        </h5>
    </div>

    <table class="table table-hover table-striped text-up">
        <tbody>
            <tr>
                <td><b>Nombre:</b><br><?php echo "$director->nombre $director->apellido" ?></td>
            </tr>

            <tr>
                <td><b>Contacto:</b><br><?php echo $director->email?"$director->email |":"" ?> <?php echo "TEL: $director->telefono" ?></td>
            </tr>

            <tr>
                <td><b>Dirección:</b><br><?php echo $director->direccion ?> - <?php echo $director->provincia->nombre ?> - <?php echo $director->pais->nombre ?></td>
            </tr>
        </tbody>
    </table>
    <div class="card-footer">
        <a href="#" class="card-link btn-editar"><i class="fas fa-edit mr-1"></i>Editar Información</a>
    </div>
</div>