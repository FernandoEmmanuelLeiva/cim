<div id='accordion'>
    <?php foreach ($institutos as $key => $o) { 
        $id = enc($o->inst_id);
    ?>
    <div class='card card-admin'>
        <div class='card-header'>
            <h4 class='card-title w-100'>
                <a class='d-block w-100 collapsed' data-toggle='collapse' href='#collapse<?php echo $id ?>' aria-expanded='false'>
                    <?php echo $o->nombre ?>
                </a>
            </h4>
        </div>
        <div id='collapse<?php echo $id ?>' data-inst="<?php echo $id ?>" class='collapse' data-parent='#accordion'>
            <div class='card-body'>
                <?php
                    @$this->load->view('institutos/info', ['instituto' => $o]);
                    echo comp("pnl-obras-collapse$id", base_url('obras/obtenerXInstituto')); 
                ?>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<script>

$('.collapse').on('shown.bs.collapse', function (e) {
   reload(`#pnl-obras-${e.currentTarget.id}`, `${$('#cert_id').val()}/${$(e.currentTarget).data('inst')}`);
})

</script>