<div class="login-box mt-0 mb-0">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <h5 class="text-center lead">¿Respresentás a una institución que participará en el Certamen?</h5><br>
            <div class="form-group ml-4">
                <div>
                    <label>
                        <input id="instituto" type='radio' name='type' value='INTITUTO'>
                        Sí
                    </label>
                </div>
                <div>
                    <label>
                        <input id="particular" type='radio' name='type' value='PARTICULAR'>
                        No, soy un participante independiente. (No represento Institución)</label>
                </div>
            </div>
            <a id="btn-no-inst" style="display:none;" class="btn btn-admin btn-block" href="<?php echo base_url('institutos/registrarIndependiente') ?>"><i class="fas fa-arrow-circle-right mr-2"></i>Continuar</a>
            <div id="form-registro" style="display:none;">
                <hr>
                <h5 class="text-center lead">Completar datos del Institución</h5>
                <?php
                echo getForm('frm-instituto', 'DATOS INSTITUTO');
                ?>
            </div>
        </div>
    </div>
</div>

<script>
    function showForm(frmHide) {
        if (frmHide){
            $('#form-registro').show();
            $('#btn-no-inst').hide();
        } 
        else {
            $('#btn-no-inst').show();
            $('#form-registro').hide();
        }
    }

    $('select').select2({
        theme: 'bootstrap4'
    })

    fillSelectOptions('#pais', '<?php echo base_url() ?>paises/options');
    $('#pais').change(function() {
        fillSelectOptions('#provincia', `<?php echo base_url() ?>paises/${this.value}/estados/options`);
    })

    $('#instituto').iCheck({
        checkboxClass: "icheckbox_flat-blue",
        radioClass: "iradio_flat-red",
    }).on('ifChanged', function(e) {
        $(this).trigger("onclick", e);
        showForm(true)
    });

    $('#particular').iCheck({
        checkboxClass: "icheckbox_flat-blue",
        radioClass: "iradio_flat-red",
    }).on('ifChanged', function(e) {
        $(this).trigger("onclick", e);
        showForm(false)
    });
</script>