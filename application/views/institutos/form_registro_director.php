<div class="login-box mt-0 mb-0">
    <div class="login-logo">
        <a href="#"><?php echo TITULO_LOGIN ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <h5 class="text-center lead">¿Usted és el Respresentante Institucional?</h5><br>
            <div class="form-group ml-4">
                <div>
                    <label>
                        <input id="director" type='radio' name='type' value='DIRECTOR'>
                        Sí
                    </label>
                </div>
                <div>
                    <label>
                        <input id="nodirector" type='radio' name='type' value='NO_DIRECTOR'>
                        No, es otra persona.</label>
                </div>
            </div>
            <a id="btn-director" style="display:none;" class="btn btn-admin btn-block" href="<?php echo base_url('institutos/registrarDirector/true') ?>"><i class="fas fa-arrow-circle-right mr-2"></i>Continuar</a>
            <div id="form-registro" style="display:none;">
                <hr>
                <h5 class="text-center lead">Completar datos del Representante</h5>
                <?php
                echo getForm('frm-persona', 'DATOS PERSONALES');
                ?>
            </div>
            <button id="btn-otro-director" class="btn btn-admin btn-block"><i class="fas fa-arrow-circle-right mr-2"></i>Continuar</button>
        </div>
    </div>
</div>

<script>
    $('#guardar').remove();

    $('#btn-otro-director').attr('disabled', true);
    $('#dni').keyup(function() {
        $('.frm-msj').html((this.value.length != 8) ? "El DNI debe tener 8 dígitos" : "");
        $('#btn-otro-director').attr('disabled', (this.value.length != 8));
    })

    function showForm(frmHide) {
        if (frmHide) {
            $('#form-registro').show();
            $('#btn-otro-director').show();
            $('#btn-director').hide();
        } else {
            $('#btn-director').show();
            $('#form-registro').hide();
            $('#btn-otro-director').hide();
        }
    }

    $('select').select2({
        theme: 'bootstrap4'
    })

    fillSelectOptions('#pais', '<?php echo base_url() ?>paises/options');
    $('#pais').change(function() {
        fillSelectOptions('#provincia', `<?php echo base_url() ?>paises/${this.value}/estados/options`);
    })

    $('#nodirector').iCheck({
        checkboxClass: "icheckbox_flat-blue",
        radioClass: "iradio_flat-red",
    }).on('ifChanged', function(e) {
        $(this).trigger("onclick", e);
        showForm(true)
    });

    $('#director').iCheck({
        checkboxClass: "icheckbox_flat-blue",
        radioClass: "iradio_flat-red",
    }).on('ifChanged', function(e) {
        $(this).trigger("onclick", e);
        showForm(false)
    });

    $('#btn-otro-director').click(function() {
        var data = getForm('#frm-persona');
        $.ajax({
            type: 'POST',
            url: `<?php echo base_url('institutos/registrarDirector') ?>`,
            data,
            success: function(res) {
                hecho();
                redirect('<?php echo base_url('admin') ?>');
            },
            error: function(res) {
                ajaxError(res);
            },
            complete: function() {

            }
        })
    });
</script>