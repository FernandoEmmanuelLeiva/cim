<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obras extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Obra');
    }

    public function dash()
    {
        dash('obras/index');
    }

    public function index($instId)
    {
        $instId =  dec($instId);
        if (!$instId) return msjError();

        $data['instId'] = $instId;
        $data['certId'] = certId();
        $this->load->view('obras/index', $data);
    }

    public function obtener($obraId)
    {
        $obraId = dec($obraId);
        if (!$obraId) {
            rspError(ERROR_ASM);
            return;
        }
        $rsp = $this->Obra->obtener($obraId);;
        if ($rsp) rspOk('OK', $rsp);
        else rspError(ERROR_ASM);
    }

    public function registrar($certId, $instId, $obraId = false)
    {
        $certId = dec($certId);
        $instId = dec($instId);
        if (!($certId && $instId)) {
            rspError(ERROR_ASM);
            return;
        }
        $post = $this->input->post();
        if ($obraId) {
            $obraId = dec($obraId);
            $post['fopa_id'] = dec($post['fopa_id']);
            $post['cate_id'] = dec($post['cate_id']);
            $rsp = $this->Obra->editar($obraId, $post);
        } else {
            $post['cert_id'] = $certId;
            $post['inst_id'] = $instId;
            $post['fopa_id'] = dec($post['fopa_id']);
            $post['cate_id'] = dec($post['cate_id']);
            $rsp = $this->Obra->guardar($post);
        }
        if ($rsp) rspOk();
        else rspError(ERROR_ASM);
    }

    public function obtenerXInstituto($certId, $instId)
    {
        $instId = dec($instId);
        $certId = dec($certId);
        if (!$instId || !$certId) {
            rspError(ERROR_ASM);
            return;
        }
        $this->load->model('Instituto');
        $data = [
            'instId' => $instId,
            'certId' => $certId
        ];

        #Datos formulario
        $data['modalidades'] = $this->Obra->obtenerModalidades();
        $data['estilos'] = $this->Obra->obtenerEstilos();
        $data['categorias'] = $this->Obra->obtenerCategorias();
        $data['formas_participacion'] = $this->Obra->obtenerFormasParticipacion();
        $data['maestros_preparadores'] = $this->Instituto->obtenerMaestros($certId, $instId);

        #Lista de obras
        $data['obras'] = $this->Obra->obtenerXInstituto($certId, $instId);
        $this->load->view('obras/tabla', $data);
    }

    public function participantes($obraId)
    {
        $obraId = dec($obraId);
        if (!$obraId) return msjError();

        $post = $this->input->post();
        if ($post) {
            $persId = dec($post['persId']);
            if (!$obraId) return msjError();
            $rsp = $this->Obra->agregarParticipante($obraId, $persId);
            if ($rsp) rspOk();
            else rspError(ERROR_ASM);
        } else {
            $this->load->model('Alumno');
            $obra = $this->Obra->obtener($obraId);
            $cate = $this->Obra->obtenerCategoria($obra->cate_id);

            #HARDCODE
            $data['alumnos'] = $this->Alumno->obtenerXEdad($obra->inst_id, 0, 100);
            // $data['alumnos'] = $this->Alumno->obtenerXEdad($obra->inst_id, $cate->min_edad, $cate->max_edad);
            
            $data['participantes'] = $this->Obra->obtenerParticipantes($obraId);
            $this->load->view('obras/tabla_participantes', $data);
        }
    }

    public function eliminar($obraId)
    {
        $obraId = dec($obraId);
        if (!$obraId) {
            rspError(ERROR_ASM);
            return;
        }
        $rsp = $this->Obra->eliminar($obraId);
        if ($rsp) rspOk();
        else rspError(ERROR_ASM);
    }

    public function eliminarParticipante($obraId, $persId)
    {
        $obraId = dec($obraId);
        $persId = dec($persId);
        if (!$obraId || !$persId) {
            rspError(ERROR_ASM);
            return;
        }
        $rsp = $this->Obra->eliminarParticipante($obraId, $persId);
        if ($rsp) rspOk();
        else rspError('No se pudo eliminar participante');
    }
}
