<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if (!sesion()) {
			logMsj('NO EXISTE SESION DE USUARIO');
			redirect('usuarios/login');
		}
	}

	public function index($loadAdmin = true)
	{
		switch (rolId()) {
			case 'ADMIN':
				$this->adminDash($loadAdmin);
				break;

			case 'USUARIO':
				$this->userDash($loadAdmin);
				break;

			default:
				logData('ROL INVALIDO');
				redirect('usuarios/login');
				break;
		}
	}

	private function userDash($loadAdmin)
	{
		logData(sesion());
		#PASO 1 | DATOS PERSONALES
		$this->load->model('Usuario');
		$userData = $this->Usuario->obtener(userId());
		if (!$userData->pers_id) {
			redirect('usuarios/datosPersonales');
			return;
		}

		#PASO 2 | DATOS INSTITUCION/PARTICULAR
		$this->load->model('Instituto');
		$rolesInsituto = $this->Usuario->obtenerRolesInstitutos(userId());

		#Si no hay datos de institutcion asociados al usuario se redirige al formulario de insitucion
		if (!$rolesInsituto->inst_id) {
			redirect('institutos/registro');
			return;
		}

		#Si es instituto es tipo INSTITUTO( no INDEPENDIENTE) validar si tiene datos del director asociado
		$this->load->model('Instituto');
		$instituto = $this->Instituto->obtener($rolesInsituto->inst_id);

		if ($instituto->type == 'INSTITUTO') {
			if ($instituto->estado == 'VALIDAR_DIRECTOR') {
				redirect('institutos/registrarDirector');
				return;
			}
		}



		$data['instId'] = $instituto->inst_id;
		#Si hay insituto registrado
		#PASO 3 | CARGA DE ALUMNOS #PASO 4 | CARGA DE OBRAS  #PASO 5 | INSCRIPCION CAPACITACIONES
		if ($loadAdmin === true) dash('dash/cliente', $data);
		else $this->load->view('dash/cliente', $data);
	}

	public function adminDash($loadAdmin)
	{
		$this->load->model('Certamen');
		$data['certamenes'] = $this->Certamen->obtener();
		if ($loadAdmin === true) dashMenu('dash/admin', $data);
		else $this->load->view('dash/admin', $data);
	}

	public function menu()
	{
		$this->load->view('layout/menu');
	}
}
