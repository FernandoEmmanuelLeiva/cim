<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paises extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Pais');
    }

    public function obtener()
    {
        $rsp = $this->Pais->obtener();
        if($rsp !== FALSE){
            $data['pais'] = $rsp;
            rspOk('OK', $data);
        }else{
            rspError(ERROR_ASM);
        }
    }

    public function options()
    {
        $rsp = $this->Pais->obtener();
        $htmlOpts = selectOpts($rsp, 'id', 'nombre');
        echo $htmlOpts;
    }

    public function obtenerProvincias($paisId)
    {
        // $paisId = enc($paisId);
        // if(!$paisId) return rspError(ERROR_401, 401);
        
        $rsp = $this->Pais->obtenerProvincias($paisId);
        if($rsp !== FALSE){
            $data['provincias'] = $rsp;
            rspOk('OK', $data);
        }else{
            rspError(ERROR_ASM);
        }
    }

    public function optionsEstados($paisId)
    {
        // $paisId = enc($paisId);
        // if(!$paisId) return rspError(ERROR_401, 401);
        
        $rsp = $this->Pais->obtenerProvincias($paisId);
        $htmlOpts = selectOpts($rsp, 'id', 'nombre');
        echo $htmlOpts; 
    }

    
}
