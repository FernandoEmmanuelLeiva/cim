<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Institutos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Instituto');
    }

    public function index($table =  false)
    {
        if (!checkRol('ADMIN')) return rspError(ERROR_401, 401);

        if (!$table) return $this->load->view('dash/institutos/index');

        $data['institutos'] = $this->Instituto->obtenerTodas();

        $this->load->view('dash/institutos/tabla', $data);
    }

    public function obtenerReporte($certId)
    {
        $certId = dec($certId);
        if (!$certId) {
            show_404();
            return;
        }
        $data['institutos'] = $this->Instituto->obtenerXCertamen($certId);
        $this->load->view('institutos/reporte', $data);
    }

    public function obtenerInfo($instId)
    {
        $instId = dec($instId);
        if (!$instId) {
            msjError();
            return;
        }
        $this->load->model('Persona');

        $instituto = $this->Instituto->obtener($instId);
        $data['instId'] = $instId;
        $data['director'] = $this->Persona->obtener($instituto->director_id);
        $data['instituto'] = $instituto;
        $this->load->view('institutos/info', $data);
    }

    public function obtener($instId)
    {
        $instId = dec($instId);
        if (!$instId) {
            msjError();
            return;
        }
        $rsp =  $this->Instituto->obtenerInfo($instId);
        if ($rsp) rspOk('OK', $rsp);
        else rspError(ERROR_ASM);
    }

    // public function obtenerObras($certId, $instId)
    // {
    //     $instId = dec($instId);
    //     $certId = dec($certId);
    //     if (!$instId || !$certId) {
    //         rspError(ERROR_ASM);
    //         return;
    //     }
    //     $this->load->model('Instituto');

    //     #Datos formulario
    //     $data['modalidades'] = $this->Obra->obtenerModalidades();
    //     $data['estilos'] = $this->Obra->obtenerEstilos();
    //     $data['tipos'] = $this->Obra->obtenerTipos();
    //     $data['categorias'] = $this->Obra->obtenerCategorias();
    //     $data['formas_participacion'] = $this->Obra->obtenerFormasParticipacion();
    //     $data['maestros_preparadores'] = $this->Instituto->obtenerMaestros($certId, $instId);

    //     #Lista de obras
    //     $data['obras'] = $this->Obra->obtenerXInstituto($certId, $instId);
    //     $this->load->view('obras/tabla', $data);
    // }

    public function registro($instId = false)
    {
        if (!sesion()) {
            redirect('usuarios/login');
            return;
        }

        $post = $this->input->post();
        if ($post) {

            if ($instId) { //Editar Datos

                $instId = dec($instId);
                if (!$instId) {
                    rspError(ERROR_ASM);
                    return;
                }

                $rsp = $this->Instituto->editar($instId, $post);
                if ($rsp) rspOk();
                else rspError(ERROR_ASM);
            } else { //Nuevo Instituto   
                $post['type'] = 'INSTITUTO';
                $post['estado'] = 'VALIDAR_DIRECTOR';
                $post['director_id'] = persId();
                $newInstituto = $this->Instituto->guardar($post);

                $this->load->model('Usuario');
                $this->Usuario->asociarInstituto(userId(), $newInstituto);
                if ($newInstituto) redirect('admin');
                else msjPage('Error Inesperado', 'Lo sentimos, reportaremos el error al administrador, vuelve a intentarno nuevamente', 'fa fa-times text-danger');
            }
        } else {
            $data['html'] = $this->load->view('institutos/form_registro', null, true);
            $this->load->view('layout/singlePage', $data);
        }
    }

    public function registrarIndependiente()
    {
        $this->load->model('Persona');
        $user = $this->Persona->obtener(persId());

        $newInstituto = array(
            'nombre' => "$user->nombre $user->apellido",
            'director_id' => persId(),
            'estado' => 'ACTIVO',
            'type' => 'INDEPENDIENTE',
        );

        $newInstId = $this->Instituto->guardar($newInstituto);
        if(!$newInstId) return msjPage('Error Inesperado', 'Lo sentimos, reportaremos el error al administrador, vuelve a intentarno nuevamente', 'fa fa-times text-danger');

        $this->load->model('Usuario');
        $res = $this->Usuario->asociarInstituto(userId(), $newInstId);
        if($res) redirect('admin');
        else msjPage('Error Inesperado', 'Lo sentimos, reportaremos el error al administrador, vuelve a intentarno nuevamente', 'fa fa-times text-danger');
        
    }

    public function registrarDirector($director = false)
    {
        $post = $this->input->post();
        if ($post || $director) {

            $this->load->model('Persona');
            $instituto = $this->Instituto->obtenerXDirectorId(persId());


            #Si eligio que el director es otra persona crea nuevo registro persona y actualizar director ID en tabla insituto
            if ($post) {
                $newPersona = validar($post, 'nombre|apellido|dni|fecha_nacimiento|telefono|pais|provincia|localidad|direccion');

                $validarPersona = $this->Persona->obtenerXDni($newPersona->dni);
                if($validarPersona)
                {
                    $persId = $validarPersona->pers_id;
                }else{
                    $persId = $this->Persona->guardar($newPersona);
                    if (!$persId) return pageError('Error Inesperado', 'Lo sentimos, reportaremos el error al administrador, vuelve a intentarno nuevamente');
                }

                $data['director_id'] = $persId;
            }

            $data['estado'] = 'ACTIVO';
            $res = $this->Instituto->editar($instituto->inst_id, $data);
            if ($res) msjPage('Registro Completo!', 'El registro se completo con éxito', 'fa fa-check text-success', 'Continuar', base_url('admin'));
            else {
                pageError('Error Inesperado', 'Lo sentimos, reportaremos el error al administrador, vuelve a intentarno nuevamente');
            }
        } else {
            $data['html'] = $this->load->view('institutos/form_registro_director', null, true);
            $this->load->view('layout/singlePage', $data);
        }
    }

    public function obtenerPaisesProvincias()
    {
        $this->load->model('Tabla');
        $data['paises'] = $this->Tabla->obtenerPaises();
        $data['provincias'] = $this->Tabla->obtenerProvincias();
        rspOk('OK', $data);
    }
}
