<?php
defined('BASEPATH') or exit('No direct script access allowed');
use setasign\Fpdi\Fpdi;

require_once('php_lib\fpdf.php');

class Test extends CI_Controller
{

	public function index()
	{
		
		$this->generatePDF("assets/template.pdf", "assets/export.pdf", "Hello world");

	}


	function generatePDF($source, $output, $text, $image = false)
	{

		$pdf = new Fpdi('Portrait', 'mm', array(215.9, 279.4)); // Array sets the X, Y dimensions in mm
		$pdf->AddPage();
		$pagecount = $pdf->setSourceFile($source);
		$tppl = $pdf->importPage(1);

		$pdf->useTemplate($tppl, null, null, 300, 300);

		if($image) $pdf->Image($image, 10, 10, 50, 50); // X start, Y start, X width, Y width in mm

		$pdf->SetFont('Helvetica', '', 10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
		$pdf->SetTextColor(0, 0, 0); // RGB 
		$pdf->SetXY(51.5, 57); // X start, Y start in mm
		$pdf->Write(0, $text);

		$pdf->Output($output, "F");
	}


	public function form($id)
	{
		$data['html'] = boxCard(form('frm-test', $this->Form->obtener($id)));
		$this->load->view('layout/singlePage', $data);
	}

	public function sesion()
	{
		$data = sesion();
		show($data);
	}
}
