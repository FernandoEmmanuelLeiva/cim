<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alumnos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Alumno');
        $this->load->model('Persona');
    }

    public function dash($instId)
    {
        $instId =  dec($instId);
        if (!$instId) return rspError('Sesion expirada', 401);
        $data['instId'] = $instId;
        dash('alumnos/index', $data);
    }

    public function index($instId)
    {
        $instId =  dec($instId);
        if (!$instId) return rspError('Sesion expirada', 401);
        $data['instId'] = $instId;
        $this->load->view('alumnos/index', $data);
    }

    
    public function obtenerXInstituto($instId)
    {
        $this->load->model('Alumno');
        $instId = dec($instId);
        if ($instId) {
            $data['instId'] = $instId;
            $data['alumnos'] = $this->Alumno->obtenerXInstituto($instId);
            $this->load->view('alumnos/lista', $data);
        } else show_404();
    }

    public function guardar($instId)
    {
        $instId =  dec($instId);
        if (!$instId) return rspError('Sesion expirada', 401);

        $post = $this->input->post();
        $newAlumno = validar($post, 'nombre|apellido|dni|edad');
        if (!$newAlumno) return rspError('Formulario Inválido', 400);

        #Validar si existe persona
        $persona = $this->Persona->obtenerXDni($newAlumno->dni);
        if (!$persona) {
            $persId = $this->Alumno->guardar($newAlumno);
            if (!$newAlumno) return rspError('Error al registrar persona');
        } else $persId = $persona->pers_id;

        #Validar si ya pertenece al insituto
        $aux = $this->Alumno->perteneceAInstituto($persId, $instId);

        if (!$aux) {
            if ($this->Alumno->asociarInstituto($persId, $instId)) {
                rspOk();
            } else {
                rspError('Error al asociar instituto');
            }
        } else {
            rspError('Alumno ya registrado', 400);
        }
    }

    public function editar($alumId)
    {
        $alumId = dec($alumId);
        if (!$alumId) return rspError('Sesion expirada', 401);

        $post = $this->input->post();
        $alumno = validar($post, 'nombre|apellido|dni|edad');

        if (!$alumno)  return rspError('Formulario Inválido', 400);

        #Si esta registrada y no es en este insituto no permite editar los datos
        if ($this->Alumno->cantidadInstitutosAsociados($alumId) > 1) return rspError('No authorizado editar', 401);

        if ($this->Alumno->editar($alumId, $alumno)) {
            rspOk();
        } else {
            rspError(ERROR_ASM);
        }
    }

    public function eliminar($alumId, $instId)
    {
        $alumId = dec($alumId);
        $instId = dec($instId);
        if (!$alumId || !$instId) return rspError('Sesion expirada', 401);

        $rsp = $this->Alumno->eliminarDeInstituto($alumId, $instId);
        if ($rsp)  rspOk();
        else rspError(ERROR_ASM);
    }

}
