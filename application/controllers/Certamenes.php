<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Certamenes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Certamen');
    }

    public function index()
    {
        $this->load->view('certamenes/index');
    }

    public function obtenerXId($certId)
    {
        $certId =  dec($certId);
        $rsp = $this->Certamen->obtenerXId($certId);
        if($rsp) rspOk('', $rsp);
        else rspError(ERROR_ASM);
    }

    public function obtenerObras($certId = false)
    {
        if(!$certId) return $this->load->view('dash/obras/index');

        $certId = dec($certId);
        if(!$certId) return show_404();

        $this->load->model('Obra');
        $data['obras'] = $this->Certamen->obtenerObras($certId);
        $this->load->view('obras/tabla', $data);
    }

    public function registro($certId = false)
    {
        $post = $this->input->post();
        if ($post) {
            if($certId){
                $certId = dec($certId);
                $rsp = $this->Certamen->editar($certId, $post);
            }else{
                
                $rsp = $this->Certamen->guardar($post);
            }
            if($rsp) rspOk(); else rspError(ERROR_ASM);

        } else {
            $this->load->view('certamenes/form');
        }
    }

    public function lista()
    {
        $data['estados'] = $this->Certamen->obtenerEstados();
        $data['certamenes'] = $this->Certamen->obtener();
        $this->load->view('certamenes/tabla', $data);
    }

    public function eliminar($certId)
    {
        $certId = dec($certId);
        $rsp = $this->Certamen->eliminar($certId);
        if($rsp){
            rspOk();
        }else rspError(ERROR_ASM);
    }

    public function estado($certId)
    {
        $certId = dec($certId);
        if(!$certId){
            rspError('Error inesperado');
            return;
        } 
        $post = $this->input->post();
        $rsp = $this->Certamen->editar($certId, $post);
        if($rsp) rspOk();
        else rspError(ERROR_ASM);
    }
}
