<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usuario');
    }

    public function index()
    {
        if (!checkRol('ADMIN')) return;

        $this->load->view('usuarios/index');
    }

    public function obtener()
    {
        $data['usuarios'] = $this->Usuario->obtenerTodas();
        $this->load->view('usuarios/tabla', $data);
    }
    public function login()
    {
        $post = $this->input->post();
        if ($post) {
            $post = validar($post, 'email|password');
            if (!$post) {
                reload();
            } else {
                $this->load->model('Usuario');
                $user = $this->Usuario->iniciarSesion($post->email, $post->password);
                if (!$user) {
                    msjPage('Usuario no registrado', 'El usuario ingresado no se encuentra registrado', 'fas fa-question-circle', 'Volver al login', base_url('usuarios/login'));
                    return;
                }
                
                sesion($user);
                redirect('admin');
            }
        } else {
            $this->load->view('usuarios/login');
        }
    }

    public function registro($token = false)
    {
        $this->load->model('Usuario');
        if ($token) {
            $userId = decToken($token);
            if ($userId) {
                if ($this->Usuario->habilitar($userId)) {
                    $user = $this->Usuario->obtener($userId);
                    msjPage('Email verificado!', 'Hola! Bienvenido al <b>Certamen de PulS</b>. Para continuar con el registro haz click en Continuar', 'fas fa-check-circle text-success', 'Continuar', base_url('admin'));
                } else {
                    echo ERROR_ASM;
                }
            } else {
                msjPage('Token expirado', 'Lo sentimos, el registro no pudo ser completado <a href="' . base_url('usuarios/registro') . '">haz click aqui</a> para intentarlo nuevamente.', 'fas fa-times');
            }
        } else {
            $post = $this->input->post();
            if ($post) {
                if (!validar($post, 'email|password')) {
                    reload();
                } else {
                    $user = $this->Usuario->obtenerXEmail($post['email']);
                    log_message('ERROR', json_encode($user));
                    if ($user) {
                        msjPage('Usuario existente', 'El usuario ingresado ya encuentra registrado', 'fas fa-exclamation-circle', 'Entendido', base_url('usuarios/login'));
                        return;
                    }

                    $res = $this->Usuario->guardarRegistro($post['email'], $post['password']);
                    if ($res) {
                        $user = $this->Usuario->obtenerXEmail($post['email']);
                        if (DEV) {
                            redirect(base_url('usuarios/validarEmail/' . encToken($user->user_id)));
                        } else {
                            msjPage('Email enviado', 'Verifica tu correo y haz click en el link para completar el registro.', 'fas fa-envelope', 'Entendido', base_url('usuarios/login'));
                        }
                    }
                }
            } else {
                $data['view'] = 'usuarios/form_registro';
                $this->load->view('layout/singlePage', $data);
            }
        }
    }

    public function validarEmail($token)
    {
        $userId = decToken($token);
        if (!$userId) return  msjPage('Token Expirado', 'El token ingresado ha expirdado', 'fas fa-exclamation-circle', 'Entendido', base_url('usuarios/login'));

        if ($this->Usuario->habilitar($userId, DEFAULT_ROL)) {
            msjPage('Email Verificado', 'Tu email se verifico con éxito.', 'fas fa-envelope', 'Entendido', base_url('usuarios/login'));
        } else {
            msjPage('Ups', 'Algo salio mal', 'fas fa-exclamation-circle', 'Entendido', base_url('usuarios/login'));
        }
    }

    public function recuperarClave($token = false)
    {
        $this->load->model('Usuario');
        if ($token) {
            $userId = decToken($token);
            if ($userId) {
                $user = $this->Usuario->obtener($userId);
                if (!$user || $user->estado != EMAIL_RECUPERACION) {
                    msjPage('Token expirado', 'Lo sentimos, el token de recuperación de acceso ha expirado <a href="' . base_url('usuarios/recuperarClave') . '">haz click aqui</a> para intentarlo nuevamente.', 'fas fa-times');
                    return;
                }

                $post = $this->input->post();
                if ($post) {
                    $res = $this->Usuario->cambiarClave($userId, $post['password']);
                    if ($res) {
                        msjPage('Contraseña Restablecida', 'Ahora puedes acceder nuevamente a tu cuenta usando la nueva contraseña ingresada', 'fas fa-check-circle text-success', 'Volver al login', base_url('usuarios/login'));
                    } else {
                        msjPage('Error Inesperado', 'Lo sentimos, la acción no pudo completarse por un error desconocido', 'fas fa-times');
                    }
                } else {
                    $data['token'] = $token;
                    $data['view'] = 'usuarios/cambiar_clave';
                    $this->load->view('layout/singlePage', $data);
                }
            } else {
                msjPage('Token expirado', 'Lo sentimos, el token de recuperación de acceso ha expirado <a href="' . base_url('usuarios/recuperarClave') . '">haz click aqui</a> para intentarlo nuevamente.', 'fas fa-times');
            }
        } else {
            $post = $this->input->post();
            if ($post) {
                $user = $this->Usuario->obtenerXEmail($post['email']);
                if (!$user) {
                    msjPage('Usuario no registrado', 'El usuario ingresado no se encuentra registrado', 'fas fa-question-circle', 'Entendido', base_url('usuarios/login'));
                    return;
                }
                $res = $this->enviarEmailRecuperacion($user);
                if ($res) msjPage('Email Enviado', 'Por favor, revisa tu correo y haz click en el link del email de recuperación', 'fas fa-envelope', 'Entendido', base_url('usuarios/login'));
                echo $res;
            } else {
                $data['view'] = 'usuarios/recuperar_clave';
                $this->load->view('layout/singlePage', $data);
            }
        }
    }

    private function enviarEmailRecuperacion($user)
    {
        $this->load->model('Usuario');
        $this->Usuario->blanquearClave($user->user_id);
        return encToken($user->user_id);
    }

    public function datosPersonales()
    {
        $post = $this->input->post();
        if ($post) {
            #Validar datos
            $userData = validar($post, 'nombre|apellido|dni|fecha_nacimiento|telefono|pais|provincia|localidad|direccion');

            #Validar si persona existe
            $this->load->model('Persona');
            $persona = $this->Persona->obtenerXDni($userData->dni);

            if($persona) {
                $persId = $persona->pers_id;
            } else {

                $sesion = sesion();
                $userData->email = $sesion['email'];
                $persId = $this->Persona->guardar($userData);

            }

            #Asociar persId al userId
            $res = $this->Usuario->editar(userId(), ['pers_id' => $persId]);
            if ($res) {
                $usuario = $this->Usuario->obtener(userId());
                sesion($usuario);
                redirect('admin');
            } else {
                #HARDCODE
                echo 'AYAYYAYA';
            }
        } else {
            $data['view'] = 'personas/form_registro';
            $this->load->view('layout/singlePage', $data);
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('usuarios/login');
    }
}
