-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: metoca
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `clie_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `direccion` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`clie_id`),
  UNIQUE KEY `clientes_un` (`dni`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (38,'Metoca','Turnos','12345678','0303456000','metoca.turnos.web@gmail.com',NULL),(39,'Fernando','Leiva','38219548','155451908','fer17916@gmail.com',NULL),(40,'Metoca','Turnos','11111111','0303456','metoca@turnosweb.com',NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuraciones`
--

DROP TABLE IF EXISTS `configuraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciones` (
  `conf_id` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` varchar(50) NOT NULL,
  `data_json` varchar(1000) DEFAULT NULL,
  `empr_id` int(11) NOT NULL,
  PRIMARY KEY (`conf_id`),
  KEY `configuraciones_fk` (`empr_id`),
  CONSTRAINT `configuraciones_fk` FOREIGN KEY (`empr_id`) REFERENCES `empresas` (`empr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuraciones`
--

LOCK TABLES `configuraciones` WRITE;
/*!40000 ALTER TABLE `configuraciones` DISABLE KEYS */;
INSERT INTO `configuraciones` VALUES (5,'dias_horas','{\"lunes\":{\"desde_am\":\"00:30\",\"hasta_am\":\"04:00\",\"desde_pm\":\"06:00\",\"hasta_pm\":\"23:30\"},\"miercoles\":{\"desde_am\":\"00:00\",\"hasta_am\":\"23:30\",\"desde_pm\":\"\",\"hasta_pm\":\"\"}}',46),(6,'general','{\"duracion_de_turnos\":\"00:30\",\"limite_de_turnos_por_cliente\":\"1\",\"limite_clientes_por_turno\":\"1\",\"limite_turnos_tiempo\":\"30\"}',46),(7,'dias_horas','{\"miercoles\":{\"desde_am\":\"00:00\",\"hasta_am\":\"23:30\",\"desde_pm\":\"\",\"hasta_pm\":\"\"}}',48),(8,'dias_horas','[]',47),(9,'general','{\"duracion_de_turnos\":\"01:30\",\"limite_de_turnos_por_cliente\":\"1\",\"limite_clientes_por_turno\":\"1\",\"limite_turnos_tiempo\":\"30\"}',47),(20,'dias_excluidos','[]',46);
/*!40000 ALTER TABLE `configuraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `empr_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `alias` varchar(20) DEFAULT NULL,
  `direccion` varchar(500) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `coordenadas` varchar(100) DEFAULT NULL,
  `estado` varchar(100) NOT NULL DEFAULT 'ACTIVO',
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`empr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` VALUES (46,'EmpresaTest','Rivadavia','EMPRT','asdasdasd','123123','fer17916@gmail.com','logotipo-para-la-barbería-con-el-polo-del-peluquero-93089856.jpg',NULL,'BAJA_TEMPORAL',0),(47,'MTC-Empresa','Rivadavia','ME','asd','asd','asd','logotipo-para-la-barbería-con-el-polo-del-peluquero-93089856.jpg',NULL,'ACTIVO',0),(48,'EmpresaTest','Capital','E1','Libertador y Mendoza','0303459','metoca.turnos.web@gmail.com','logotipo-para-la-barbería-con-el-polo-del-peluquero-93089856.jpg',NULL,'BAJA_TEMPORAL',0);
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formularios`
--

DROP TABLE IF EXISTS `formularios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formularios` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `fec_alta` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formularios`
--

LOCK TABLES `formularios` WRITE;
/*!40000 ALTER TABLE `formularios` DISABLE KEYS */;
INSERT INTO `formularios` VALUES (1,'Reservar Turno','2020-07-30 16:33:33',0),(2,'Detalle Turno','2020-07-31 20:44:37',0),(3,'Empresa','2020-09-04 18:26:03',0),(4,'Posturno','2020-10-25 22:11:16',0),(5,'precios','2020-10-25 23:12:05',0);
/*!40000 ALTER TABLE `formularios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frm_instancias`
--

DROP TABLE IF EXISTS `frm_instancias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_instancias` (
  `inst_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(100) DEFAULT NULL,
  `value` varchar(300) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `icon` varchar(20) DEFAULT NULL,
  `col` int(11) NOT NULL DEFAULT 12,
  `orden` int(11) DEFAULT NULL,
  `info_id` int(10) unsigned NOT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`,`inst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_instancias`
--

LOCK TABLES `frm_instancias` WRITE;
/*!40000 ALTER TABLE `frm_instancias` DISABLE KEYS */;
/*!40000 ALTER TABLE `frm_instancias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frm_items`
--

DROP TABLE IF EXISTS `frm_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_items` (
  `form_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `label` varchar(100) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `icon` varchar(100) DEFAULT NULL,
  `col` int(11) NOT NULL DEFAULT 12,
  `orden` int(11) DEFAULT NULL,
  `class` varchar(100) DEFAULT NULL,
  `tipo` varchar(20) NOT NULL DEFAULT 'input',
  `valores` varchar(100) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`,`form_id`),
  KEY `frm_plantillas_fk` (`form_id`),
  CONSTRAINT `frm_plantillas_fk` FOREIGN KEY (`form_id`) REFERENCES `formularios` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_items`
--

LOCK TABLES `frm_items` WRITE;
/*!40000 ALTER TABLE `frm_items` DISABLE KEYS */;
INSERT INTO `frm_items` VALUES (4,'adicionales','Adiccionales:',0,NULL,12,4,NULL,'checkbox','adicionales','total#adicional'),(3,'alias',NULL,1,'fa-at',12,20,NULL,'input',NULL,NULL),(1,'apellido',NULL,1,'fa-font',12,30,'data','input',NULL,NULL),(3,'direccion',NULL,1,'fa-map-marker-alt',12,30,NULL,'input',NULL,NULL),(1,'dni',NULL,1,'fa-hashtag w-icon',12,10,NULL,'number',NULL,NULL),(2,'dni',NULL,0,'fa-hashtag',12,10,NULL,'input',NULL,NULL),(1,'email',NULL,0,'fa-envelope',12,50,'data','input',NULL,NULL),(2,'email',NULL,0,'fa-envelope',12,40,NULL,'input',NULL,NULL),(3,'email',NULL,1,'fa-envelope',12,50,NULL,'input',NULL,NULL),(3,'empr_id',NULL,0,NULL,12,NULL,'hidden','input',NULL,NULL),(3,'localidad',NULL,1,'fa-map',12,21,NULL,'input',NULL,NULL),(1,'nombre',NULL,1,'fa-user',12,20,'data','input',NULL,NULL),(3,'nombre',NULL,1,'fa-asterisk',12,10,NULL,'input',NULL,NULL),(2,'nombre_apellido','Nombre y Apellido',0,'fa-user',12,20,NULL,'input',NULL,NULL),(1,'telefono',NULL,1,'fa-phone',12,40,'data','number',NULL,NULL),(2,'telefono',NULL,0,'fa-phone',12,30,NULL,'input',NULL,NULL),(3,'telefono',NULL,1,'fa-phone',12,40,NULL,'input',NULL,NULL),(4,'tipo_corte','Seleccionar tipo de corte:',1,NULL,12,2,NULL,'radio','tipos_cortes','total#tipo_corte'),(1,'token',NULL,1,NULL,12,2,'hidden','input',NULL,NULL);
/*!40000 ALTER TABLE `frm_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frm_valores`
--

DROP TABLE IF EXISTS `frm_valores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_valores` (
  `form_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `valor` varchar(500) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `info_id` int(11) NOT NULL,
  PRIMARY KEY (`form_id`,`name`,`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_valores`
--

LOCK TABLES `frm_valores` WRITE;
/*!40000 ALTER TABLE `frm_valores` DISABLE KEYS */;
/*!40000 ALTER TABLE `frm_valores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenes_empresas`
--

DROP TABLE IF EXISTS `imagenes_empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenes_empresas` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `vertical` tinyint(1) DEFAULT NULL,
  `empr_id` int(11) NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenes_empresas`
--

LOCK TABLES `imagenes_empresas` WRITE;
/*!40000 ALTER TABLE `imagenes_empresas` DISABLE KEYS */;
INSERT INTO `imagenes_empresas` VALUES (2,'0e79af19b364e2eee854c4ac202cb2722.jpg',1,46);
/*!40000 ALTER TABLE `imagenes_empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificaciones_usuarios`
--

DROP TABLE IF EXISTS `notificaciones_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificaciones_usuarios` (
  `nous_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fecha_hora_programada` datetime NOT NULL,
  `visto` tinyint(1) NOT NULL DEFAULT 0,
  `noti_id` varchar(50) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `mensaje` varchar(100) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nous_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificaciones_usuarios`
--

LOCK TABLES `notificaciones_usuarios` WRITE;
/*!40000 ALTER TABLE `notificaciones_usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificaciones_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registros_licencias`
--

DROP TABLE IF EXISTS `registros_licencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registros_licencias` (
  `repa_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `cantidad_empresas` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`repa_id`,`user_id`,`prod_id`),
  KEY `registros_licencias_fk` (`user_id`),
  KEY `registros_licencias_fk_2` (`prod_id`),
  CONSTRAINT `registros_licencias_fk` FOREIGN KEY (`user_id`) REFERENCES `usuarios` (`user_id`),
  CONSTRAINT `registros_licencias_fk_1` FOREIGN KEY (`repa_id`) REFERENCES `registros_pagos` (`repa_id`),
  CONSTRAINT `registros_licencias_fk_2` FOREIGN KEY (`prod_id`) REFERENCES `sys_productos` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registros_licencias`
--

LOCK TABLES `registros_licencias` WRITE;
/*!40000 ALTER TABLE `registros_licencias` DISABLE KEYS */;
INSERT INTO `registros_licencias` VALUES (11,43,4,'VENCIDO','2019-11-11','2019-11-11',1,'2020-11-14 19:05:51',0),(12,43,4,'ACTIVO','2020-11-11','2020-12-12',2,'2020-11-12 19:36:19',0),(13,44,4,'VENCIDO','2020-11-14','2020-11-28',1,'2020-11-14 21:21:21',0);
/*!40000 ALTER TABLE `registros_licencias` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER cambiarestadoempresas
AFTER update 
ON registros_licencias FOR EACH row

begin 
if new.estado = 'VENCIDO' THEN
update metoca.empresas set estado = 'BAJA'
where empr_id in (
	select empr_id  from metoca.rel_usuarios_empresas rue where rue.user_id = new.user_id and rue.rol_id = 'ADMIN'
); end IF;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `registros_pagos`
--

DROP TABLE IF EXISTS `registros_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registros_pagos` (
  `repa_id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(100) NOT NULL DEFAULT 'ACTIVO',
  `transaccion` varchar(500) NOT NULL,
  `meses` int(11) NOT NULL,
  `total` float NOT NULL,
  `detalle` varchar(1000) DEFAULT NULL,
  `data_json` varchar(1000) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`repa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registros_pagos`
--

LOCK TABLES `registros_pagos` WRITE;
/*!40000 ALTER TABLE `registros_pagos` DISABLE KEYS */;
INSERT INTO `registros_pagos` VALUES (11,'ACTIVO','MANUAL',1,0,'{\"prod_id\":\"4\",\"nombre\":\"Prueba Gratis\",\"descripcion\":\"free\",\"precio_mes\":\"0\",\"cantidad_empresas\":\"1\",\"descuentos\":\"0\",\"incrementos\":\"0\",\"data_json\":null,\"date\":\"2020-10-13 23:09:02\",\"delete\":\"0\",\"incrementos_total\":\"0\",\"descuentos_total\":\"0\"}',NULL,'2020-10-14 00:08:29',0),(12,'ACTIVO','MANUAL',1,0,'{\"prod_id\":\"4\",\"nombre\":\"Prueba Gratis\",\"descripcion\":\"free\",\"precio_mes\":\"0\",\"cantidad_empresas\":\"1\",\"descuentos\":\"0\",\"incrementos\":\"0\",\"data_json\":null,\"date\":\"2020-10-13 23:09:02\",\"delete\":\"0\",\"incrementos_total\":\"0\",\"descuentos_total\":\"0\"}',NULL,'2020-11-12 19:36:19',0),(13,'ACTIVO','MANUAL',1,0,'{\"prod_id\":\"4\",\"nombre\":\"Prueba Gratis\",\"descripcion\":\"free\",\"precio_mes\":\"0\",\"cantidad_empresas\":\"1\",\"descuentos\":\"0\",\"incrementos\":\"0\",\"data_json\":null,\"date\":\"2020-10-13 23:09:02\",\"delete\":\"0\",\"incrementos_total\":\"0\",\"descuentos_total\":\"0\"}',NULL,'2020-11-14 21:21:21',0);
/*!40000 ALTER TABLE `registros_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registros_tokens`
--

DROP TABLE IF EXISTS `registros_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registros_tokens` (
  `reto_id` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` varchar(100) NOT NULL,
  `data_json` varchar(500) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`reto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registros_tokens`
--

LOCK TABLES `registros_tokens` WRITE;
/*!40000 ALTER TABLE `registros_tokens` DISABLE KEYS */;
INSERT INTO `registros_tokens` VALUES (11,'INVITACION_EMPRESA','{\"username\":\"FernandoLeiva\",\"email\":\"fer17916@gmail.com\",\"password\":\"827ccb0eea8a706c4c34a16891f84e7b\"}','2020-11-07 19:32:58',1),(12,'INVITACION_EMPRESA','{\"username\":\"FernandoLeiva\",\"email\":\"fer17916@gmail.com\",\"password\":\"827ccb0eea8a706c4c34a16891f84e7b\"}','2020-11-10 21:31:03',1),(13,'INVITACION_EMPRESA','{\"username\":\"Metoca\",\"email\":\"metoca.turnos.web@gmail.com\",\"password\":\"827ccb0eea8a706c4c34a16891f84e7b\"}','2020-11-14 21:16:45',1),(14,'INVITACION_EMPRESA','{\"email\":\"fer17916@gmail.com\",\"rol_id\":\"EMP_ADMIN\",\"empr_id\":47,\"empresa\":\"MTC-Empresa\"}','2020-11-19 19:46:37',1),(15,'INVITACION_EMPRESA','{\"email\":\"metoca.turnos.web@gmail.com\",\"rol_id\":\"EMPLEADO\",\"empr_id\":46,\"empresa\":\"EmpresaTest\"}','2020-11-19 21:23:59',1);
/*!40000 ALTER TABLE `registros_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rel_formularios_empresas`
--

DROP TABLE IF EXISTS `rel_formularios_empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_formularios_empresas` (
  `form_id` int(11) NOT NULL,
  `empr_id` int(11) NOT NULL,
  PRIMARY KEY (`form_id`,`empr_id`),
  CONSTRAINT `formularios_empresas_fk` FOREIGN KEY (`form_id`) REFERENCES `formularios` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rel_formularios_empresas`
--

LOCK TABLES `rel_formularios_empresas` WRITE;
/*!40000 ALTER TABLE `rel_formularios_empresas` DISABLE KEYS */;
/*!40000 ALTER TABLE `rel_formularios_empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rel_turnos_clientes`
--

DROP TABLE IF EXISTS `rel_turnos_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_turnos_clientes` (
  `turn_id` int(11) NOT NULL,
  `clie_id` int(11) NOT NULL,
  PRIMARY KEY (`turn_id`,`clie_id`),
  KEY `turnos_clientes_fk` (`clie_id`),
  CONSTRAINT `turnos_clientes_fk` FOREIGN KEY (`clie_id`) REFERENCES `clientes` (`clie_id`),
  CONSTRAINT `turnos_clientes_fk_1` FOREIGN KEY (`turn_id`) REFERENCES `turnos` (`turn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rel_turnos_clientes`
--

LOCK TABLES `rel_turnos_clientes` WRITE;
/*!40000 ALTER TABLE `rel_turnos_clientes` DISABLE KEYS */;
INSERT INTO `rel_turnos_clientes` VALUES (312,38),(328,38),(330,38),(331,39),(332,40),(333,40);
/*!40000 ALTER TABLE `rel_turnos_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rel_usuarios_empresas`
--

DROP TABLE IF EXISTS `rel_usuarios_empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_usuarios_empresas` (
  `estado` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rol_id` varchar(15) NOT NULL,
  `empr_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`rol_id`,`empr_id`),
  KEY `rel_usuarios_empresas_fk` (`user_id`),
  KEY `rel_usuarios_empresas_fk_1` (`empr_id`),
  KEY `rel_usuarios_empresas_fk_2` (`rol_id`),
  CONSTRAINT `rel_usuarios_empresas_fk` FOREIGN KEY (`user_id`) REFERENCES `usuarios` (`user_id`),
  CONSTRAINT `rel_usuarios_empresas_fk_1` FOREIGN KEY (`empr_id`) REFERENCES `empresas` (`empr_id`),
  CONSTRAINT `rel_usuarios_empresas_fk_2` FOREIGN KEY (`rol_id`) REFERENCES `sys_roles` (`rol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rel_usuarios_empresas`
--

LOCK TABLES `rel_usuarios_empresas` WRITE;
/*!40000 ALTER TABLE `rel_usuarios_empresas` DISABLE KEYS */;
INSERT INTO `rel_usuarios_empresas` VALUES ('ACTIVO',43,'ADMIN',46),('ACTIVO',43,'ADMIN',48),('ACTIVO',43,'EMPLEADO',47),('ACTIVO',44,'ADMIN',47),('ACTIVO',44,'EMPLEADO',46);
/*!40000 ALTER TABLE `rel_usuarios_empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_notificaciones`
--

DROP TABLE IF EXISTS `sys_notificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_notificaciones` (
  `noti_id` varchar(50) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `mensaje` varchar(100) DEFAULT NULL,
  `icon` varchar(100) NOT NULL DEFAULT 'icon_turno.png',
  `link` varchar(300) DEFAULT NULL,
  `minutos_antes` int(11) DEFAULT 0,
  PRIMARY KEY (`noti_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_notificaciones`
--

LOCK TABLES `sys_notificaciones` WRITE;
/*!40000 ALTER TABLE `sys_notificaciones` DISABLE KEYS */;
INSERT INTO `sys_notificaciones` VALUES ('NF1H','No llegues tarde!','Falta 1 hora para tu turno.','icon_turno.png',NULL,60),('NTNOW','Turno en %s','Hora: %s . Confirma tu asistencia','icon_turno.png',NULL,0);
/*!40000 ALTER TABLE `sys_notificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_productos`
--

DROP TABLE IF EXISTS `sys_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_productos` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `precio_mes` float NOT NULL,
  `cantidad_empresas` int(11) NOT NULL,
  `descuentos` float NOT NULL DEFAULT 1,
  `incrementos` float NOT NULL DEFAULT 1,
  `data_json` varchar(1000) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_productos`
--

LOCK TABLES `sys_productos` WRITE;
/*!40000 ALTER TABLE `sys_productos` DISABLE KEYS */;
INSERT INTO `sys_productos` VALUES (1,'Pack 1',NULL,350,1,0,26,NULL,'2020-10-07 12:45:19',0),(2,'Pack 2',NULL,700,3,0,26,NULL,'2020-10-07 12:45:19',0),(3,'Pack 3',NULL,1050,100,0,26,NULL,'2020-10-07 12:45:19',0),(4,'Prueba Gratis','free',0,1,0,0,NULL,'2020-10-13 23:09:02',0);
/*!40000 ALTER TABLE `sys_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_roles`
--

DROP TABLE IF EXISTS `sys_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_roles` (
  `rol_id` varchar(15) NOT NULL,
  `permisos_menu` varchar(500) DEFAULT NULL,
  `permisos_views` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`rol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_roles`
--

LOCK TABLES `sys_roles` WRITE;
/*!40000 ALTER TABLE `sys_roles` DISABLE KEYS */;
INSERT INTO `sys_roles` VALUES ('ADMIN',NULL,NULL),('EMPLEADO',NULL,NULL),('EMP_ADMIN',NULL,NULL);
/*!40000 ALTER TABLE `sys_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tablas`
--

DROP TABLE IF EXISTS `tablas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tablas` (
  `tabl_id` int(11) NOT NULL AUTO_INCREMENT,
  `tabla` varchar(100) NOT NULL,
  `valor` varchar(100) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `empr_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`tabl_id`),
  KEY `tablas_fk` (`empr_id`),
  CONSTRAINT `tablas_fk` FOREIGN KEY (`empr_id`) REFERENCES `empresas` (`empr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tablas`
--

LOCK TABLES `tablas` WRITE;
/*!40000 ALTER TABLE `tablas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tablas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turnos`
--

DROP TABLE IF EXISTS `turnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turnos` (
  `turn_id` int(11) NOT NULL AUTO_INCREMENT,
  `inicio` datetime NOT NULL,
  `fin` datetime DEFAULT NULL,
  `duracion_minutos` int(11) NOT NULL,
  `detalle` varchar(300) NOT NULL,
  `delete` tinyint(1) DEFAULT 0,
  `estado` varchar(10) NOT NULL DEFAULT 'ACTIVO',
  `cupo` int(11) NOT NULL,
  `token` varchar(200) NOT NULL,
  `empr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`turn_id`),
  KEY `turnos_fk` (`empr_id`),
  CONSTRAINT `turnos_fk` FOREIGN KEY (`empr_id`) REFERENCES `empresas` (`empr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=334 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turnos`
--

LOCK TABLES `turnos` WRITE;
/*!40000 ALTER TABLE `turnos` DISABLE KEYS */;
INSERT INTO `turnos` VALUES (312,'2020-11-30 21:30:00','2020-11-30 22:00:00',0,'Metoca Turnos - DNI: 12345678',0,'ACTIVO',0,'Vkjqpnjnea',46),(328,'1969-12-31 21:05:00','1969-12-31 22:35:00',0,'Metoca Turnos - DNI: 12345678',0,'ACTIVO',0,'17X9AJmaOz',48),(330,'2020-12-09 00:00:00','2020-12-09 01:30:00',0,'Metoca Turnos - DNI: 12345678',0,'ACTIVO',0,'VkjqpdDeVa',48),(331,'2020-12-23 23:00:00','2020-12-23 23:30:00',0,'Fernando Leiva - DNI: 38219548',0,'CONFIRMADO',0,'Vkjqgqd2pa',46),(332,'2020-12-28 00:30:00','2020-12-28 01:00:00',0,'Metoca Turnos - DNI: 11111111',0,'ACTIVO',0,'VkjqgAVLVa',46),(333,'2020-12-30 16:00:00','2020-12-30 16:30:00',0,'Metoca Turnos - DNI: 11111111',0,'ACTIVO',0,'VkjqgenXNa',46);
/*!40000 ALTER TABLE `turnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `clie_id` int(11) DEFAULT NULL,
  `estado` varchar(100) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `usuarios_un` (`email`),
  KEY `usuarios_fk_2` (`clie_id`),
  CONSTRAINT `usuarios_fk_2` FOREIGN KEY (`clie_id`) REFERENCES `clientes` (`clie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (43,'FernandoLeiva','827ccb0eea8a706c4c34a16891f84e7b','fer17916@gmail.com',NULL,'ACTIVO'),(44,'Metoca','b0baee9d279d34fa1dfd71aadb908c3f','metoca.turnos.web@gmail.com',38,'ACTIVO');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_redes_sociales`
--

DROP TABLE IF EXISTS `usuarios_redes_sociales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_redes_sociales` (
  `user_id` int(11) NOT NULL,
  `reso_id` varchar(100) NOT NULL,
  `servicio` varchar(100) NOT NULL,
  `data_json` varchar(1000) NOT NULL,
  `foto_perfil` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`reso_id`,`servicio`),
  CONSTRAINT `usuarios_redes_sociales_fk` FOREIGN KEY (`user_id`) REFERENCES `usuarios` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_redes_sociales`
--

LOCK TABLES `usuarios_redes_sociales` WRITE;
/*!40000 ALTER TABLE `usuarios_redes_sociales` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_redes_sociales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'metoca'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05 19:30:14
