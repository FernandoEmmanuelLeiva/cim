-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: cim
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnos` (
  `alum_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `dni` varchar(8) NOT NULL,
  `edad` int(11) NOT NULL,
  `inst_id` int(11) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT 0,
  `maestro_preparador` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`alum_id`),
  UNIQUE KEY `alumnos_un` (`dni`),
  KEY `alumnos_fk` (`inst_id`),
  CONSTRAINT `alumnos_fk` FOREIGN KEY (`inst_id`) REFERENCES `instituciones` (`inst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumnos`
--

LOCK TABLES `alumnos` WRITE;
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` VALUES (1,'Fernando','Leiva','38219548',25,1,0,0),(2,'Nombre 1','Apellido 1','11111111',25,5,0,0),(3,'Nombre 2','Apellido 2','22222222',12,5,0,0),(4,'Nombre 3','Apellido 3','33333333',34,5,0,0),(5,'Nombre 4','Apellido 4','44444444',45,5,0,0),(6,'Emmanuel','Leiva','12345678',25,1,0,0),(7,'Pepito','Galvez','12345679',11,1,0,0),(8,'gabriel ','almada','37455612',26,1,0,0);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capacitaciones`
--

DROP TABLE IF EXISTS `capacitaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capacitaciones` (
  `capa_id` int(11) NOT NULL AUTO_INCREMENT,
  `modalidad` varchar(100) NOT NULL,
  `temario` varchar(200) DEFAULT NULL,
  `maestro` varchar(200) DEFAULT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`capa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capacitaciones`
--

LOCK TABLES `capacitaciones` WRITE;
/*!40000 ALTER TABLE `capacitaciones` DISABLE KEYS */;
INSERT INTO `capacitaciones` VALUES (7,'ESPAÑOL','“BAILE FLAMENCO, TALLER DE ENTRENAMIENTO PARA EL BAILE.”','GABRIEL ARANGO',600),(8,'FOLKLORE ARGENTINO','“VARIANTES ARGENTINAS DE LA ZAMACUECA”','FEDERICO BOGADO',400),(9,'ÁRABE','“ORIENTAL MODERNO”','DAVID ABRAHAM',600),(10,'ÁRABE','“DABKE ESTILO BAALBAKIIEH”','FARID YAMIN',500),(11,'RITMOS LATINOS','“TALLER DE MAMBO SHINES (PASOS SUELTOS) – CON ELEMENTOS','NOELIA MINATTO',500),(12,'RITMOS URBANOS','“HIP HOP FUSIÓN”','ESTEBAN TOPPI',500);
/*!40000 ALTER TABLE `capacitaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frm_formularios`
--

DROP TABLE IF EXISTS `frm_formularios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_formularios` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `descripcion` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `empr_id` int(11) DEFAULT NULL,
  `fec_alta` datetime DEFAULT current_timestamp(),
  `eliminado` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_formularios`
--

LOCK TABLES `frm_formularios` WRITE;
/*!40000 ALTER TABLE `frm_formularios` DISABLE KEYS */;
INSERT INTO `frm_formularios` VALUES (1,'Datos Personales','-',NULL,'2019-08-17 14:24:38',0),(2,'Información Médica',NULL,NULL,'2019-11-19 20:02:04',0),(3,'Informacion de Contacto',NULL,NULL,'2019-11-20 01:19:00',0),(23,'Form Test',NULL,NULL,'2019-12-07 19:16:05',0),(24,'Form Test 3',NULL,NULL,'2019-12-07 19:17:57',0),(25,'CIM - Capacitaciones',NULL,NULL,'2019-12-20 23:32:16',0),(26,'CIM - Inscripción Certamen de Danzas',NULL,NULL,'2019-12-21 10:11:11',0),(27,'CIM - Inscripción Certamen de Danzas - Obras',NULL,NULL,'2019-12-21 10:32:05',0);
/*!40000 ALTER TABLE `frm_formularios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frm_instancias_formularios`
--

DROP TABLE IF EXISTS `frm_instancias_formularios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_instancias_formularios` (
  `item_id` int(11) NOT NULL,
  `info_id` int(11) NOT NULL,
  `valor` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fec_alta` datetime DEFAULT current_timestamp(),
  `eliminado` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`item_id`,`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_instancias_formularios`
--

LOCK TABLES `frm_instancias_formularios` WRITE;
/*!40000 ALTER TABLE `frm_instancias_formularios` DISABLE KEYS */;
/*!40000 ALTER TABLE `frm_instancias_formularios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frm_items`
--

DROP TABLE IF EXISTS `frm_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `requerido` tinyint(4) DEFAULT NULL,
  `valo_id` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `tipo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `fec_alta` datetime DEFAULT current_timestamp(),
  `eliminado` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_items`
--

LOCK TABLES `frm_items` WRITE;
/*!40000 ALTER TABLE `frm_items` DISABLE KEYS */;
INSERT INTO `frm_items` VALUES (26,'Complete todos los campos del Obligatorios *',NULL,NULL,NULL,1,'comentario',10,'2019-08-17 14:27:38',0),(27,'Nombre','nombre',1,NULL,1,'input',20,'2019-08-17 14:28:46',0),(28,'Apellido','apellido',1,NULL,1,'input',30,'2019-08-17 14:28:46',0),(29,'Fecha Nacimiento','fecha_nacimiento',1,NULL,1,'date',40,'2019-08-17 14:32:37',0),(30,'Email','email',1,NULL,1,'input',50,'2019-08-17 14:34:08',0),(31,'Seleccionar Provincia','provincia',1,'provincias',1,'select',60,'2019-08-17 14:34:57',1),(32,'Seleccionar Sexo','sexo',1,'sexos',1,'radio',70,'2019-08-17 15:40:06',0),(33,'Seleccionar Opcion','contrato',1,'contratos',1,'checkgroup',80,'2019-08-17 15:40:06',1),(34,'Adjuntar Archivo','pdf',1,NULL,1,'file',90,'2019-08-17 15:42:37',1),(35,'Identificador Único','key',1,NULL,1,'input',11,'2019-08-17 15:42:37',0),(36,'Grupo Sanguíneo','grupo_sanguineo',NULL,NULL,2,'input',10,'2019-11-19 20:15:01',0),(37,'Medicamentos','medicamentos',NULL,NULL,2,'textarea',20,'2019-11-19 20:15:01',0),(38,'Operaciones','operaciones',NULL,NULL,2,'textarea',30,'2019-11-19 20:15:01',0),(39,'Requiere Atención Especial','atencion_especial',NULL,NULL,2,'check',9,'2019-11-19 20:15:01',0),(40,'Observaciones','observacion',NULL,NULL,2,'textarea',40,'2019-11-19 20:15:01',0),(41,'* Información Médica del cliente importante a tener encuenta para una mejor atención en caso de Emergencia. *',NULL,NULL,NULL,2,'comentario',1,'2019-11-19 22:11:08',0),(42,'Domicilio','domicilio',NULL,NULL,3,'textarea',20,'2019-11-20 01:17:54',0),(43,'Celular','celular',NULL,NULL,3,'input',10,'2019-11-20 01:17:54',0),(44,'Teléfono Fijo','fijo',NULL,NULL,3,'input',11,'2019-11-20 01:17:54',0),(45,'Contacto Adicional en Caso de Emergencia','contacto_adicional',NULL,NULL,3,'textarea',12,'2019-12-02 19:42:13',0),(46,'* El Identificador Único servirá para identificar univocamente a tus clientes y no se podrá repetir. (Recomandos usar D.N.I o Número de Legajo, etc). *',NULL,NULL,NULL,1,'comentario',12,'2019-12-02 20:32:50',0),(47,'* Teléfonos de Familiariares,  Amigos, Tutores, etc... para contactar e  informar cualquier eventualidad. *',NULL,NULL,NULL,3,'comentario',13,'2019-12-02 21:14:29',0),(48,'* La información de tus clientes será guardada de manera confidencial  *',NULL,NULL,NULL,3,'comentario',9,'2019-12-02 21:17:56',0),(49,'Soy un comentario','',NULL,NULL,23,'comentario',70,'2019-12-07 19:41:14',0),(50,'Nombre','nombre',NULL,NULL,23,'input',60,'2019-12-07 19:45:40',0),(51,'Apellido','apellido',NULL,NULL,23,'input',50,'2019-12-07 19:46:43',0),(52,'Telefono','telefono',NULL,NULL,23,'input',40,'2019-12-07 19:50:57',0),(53,'Email','email',NULL,NULL,23,'input',30,'2019-12-07 19:51:16',0),(54,'Domicilio','domicilio',NULL,NULL,23,'input',20,'2019-12-07 19:53:27',0),(55,'DNI','dni',NULL,NULL,23,'input',10,'2019-12-07 19:55:43',0),(56,'Fecha','nacimiento',0,NULL,23,'date',80,'2019-12-08 14:02:44',0),(57,'Fecha','fecha',0,NULL,23,'date',0,'2019-12-08 14:04:05',0),(58,'Foto','foto',1,NULL,23,'file',90,'2019-12-08 14:04:29',0),(59,'Nombre','nombre',1,NULL,25,'input',10,'2019-12-20 23:33:58',0),(60,'Apellido','apellido',1,NULL,25,'input',20,'2019-12-20 23:34:32',0),(62,'Fecha Nacimiento','fecha_nacimiento',1,NULL,25,'date',30,'2019-12-20 23:36:46',0),(63,'D.N.I','dni',1,NULL,25,'input',40,'2019-12-20 23:37:09',0),(64,'Dirección','direccion',1,NULL,25,'textarea',80,'2019-12-20 23:37:50',0),(65,'Localidad','localidad',1,NULL,25,'input',70,'2019-12-20 23:38:49',0),(66,'Provincia','provincia',1,NULL,25,'select',60,'2019-12-20 23:39:07',0),(67,'País','pais',1,NULL,25,'select',50,'2019-12-20 23:43:42',0),(68,'Cantidad de Cursos','cantidad_cursos',1,NULL,25,'input',100,'2019-12-20 23:51:14',0),(69,'Modalidades','modalidad',1,NULL,25,'select',105,'2019-12-20 23:56:26',0),(70,'Total a Pagar','total',1,NULL,25,'input',110,'2019-12-20 23:58:18',0),(72,'Nombre','nombre',1,NULL,26,'input',20,'2019-12-21 10:15:05',0),(73,'Dirección','direccion',1,NULL,26,'textarea',30,'2019-12-21 10:17:32',0),(74,'Director','director',1,NULL,26,'input',22,'2019-12-21 10:18:44',0),(77,'Teléfono','telefono',1,NULL,26,'input',24,'2019-12-21 10:27:42',0),(78,'Email','email',1,NULL,26,'input',23,'2019-12-21 10:27:51',0),(80,'Localidad','localidad',1,NULL,26,'input',100,'2019-12-21 10:30:09',0),(81,'Provincia','provincia',1,NULL,26,'input',110,'2019-12-21 10:30:33',0),(82,'País','pais',1,NULL,26,'input',120,'2019-12-21 10:30:45',0),(84,'Nombre','nombre_obra',1,NULL,27,'input',20,'2019-12-21 10:33:08',0),(85,'Duración','duracion',1,NULL,27,'input',30,'2019-12-21 10:33:43',0),(86,'Categoría','categoria',1,'categoria',27,'select',51,'2019-12-21 10:44:48',0),(88,'Formar de Participación','forma_participacion',1,'forma_preparacion',27,'select',60,'2019-12-21 10:45:50',0),(89,'Maestro Preparador','maestro_preparador',1,NULL,27,'input',70,'2019-12-21 10:46:15',0),(90,'Contraseña','clave',1,NULL,26,'input',121,'2020-03-01 23:37:10',0),(91,'Repita Contraseña','rclave',1,NULL,26,'input',122,'2020-03-01 23:37:10',0);
/*!40000 ALTER TABLE `frm_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `in_ca`
--

DROP TABLE IF EXISTS `in_ca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_ca` (
  `inca_id` int(11) NOT NULL,
  `capa_id` int(11) NOT NULL,
  PRIMARY KEY (`inca_id`,`capa_id`),
  KEY `in_ca_fk` (`capa_id`),
  CONSTRAINT `in_ca_fk` FOREIGN KEY (`capa_id`) REFERENCES `capacitaciones` (`capa_id`),
  CONSTRAINT `in_ca_fk_1` FOREIGN KEY (`inca_id`) REFERENCES `inscriptos_capacitacion` (`inca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_ca`
--

LOCK TABLES `in_ca` WRITE;
/*!40000 ALTER TABLE `in_ca` DISABLE KEYS */;
INSERT INTO `in_ca` VALUES (7,7),(7,8),(7,9),(7,10),(7,11),(7,12);
/*!40000 ALTER TABLE `in_ca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscriptos_capacitacion`
--

DROP TABLE IF EXISTS `inscriptos_capacitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscriptos_capacitacion` (
  `inca_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `fecha_nacimiento` varchar(100) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `localidad` varchar(100) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `total` float NOT NULL,
  `inst_id` int(11) NOT NULL,
  PRIMARY KEY (`inca_id`),
  UNIQUE KEY `inscriptos_capacitacion_un` (`dni`),
  KEY `inscriptos_capacitacion_fk` (`inst_id`),
  CONSTRAINT `inscriptos_capacitacion_fk` FOREIGN KEY (`inst_id`) REFERENCES `instituciones` (`inst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscriptos_capacitacion`
--

LOCK TABLES `inscriptos_capacitacion` WRITE;
/*!40000 ALTER TABLE `inscriptos_capacitacion` DISABLE KEYS */;
INSERT INTO `inscriptos_capacitacion` VALUES (7,'Sergio','Petini','38219548','03/23/2020','Argentina','San Juan','Santa Lucia','Belgrano 798 SUR',3100,3);
/*!40000 ALTER TABLE `inscriptos_capacitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instituciones`
--

DROP TABLE IF EXISTS `instituciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instituciones` (
  `inst_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `dni_director` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `localidad` varchar(200) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`inst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instituciones`
--

LOCK TABLES `instituciones` WRITE;
/*!40000 ALTER TABLE `instituciones` DISABLE KEYS */;
INSERT INTO `instituciones` VALUES (1,'Nombre Test','Director Test','fer17916@gmail.com','155545555','Belgrano 798 SUR','Santa Lucia','San Juan','Argentina','2021-02-01 21:57:59',0),(2,'Instituto 1','Director1','instituto1@gmail.com','123','Belgrano 798 SUR','Santa Lucia','San Juan','Argentina','2021-02-01 21:57:59',0),(3,'Instituto Ejemplo','Director Ejemplo','ejemplo@mail.com','1555555','Direccion Ejemplo','Capital','San Juan','Argentina','2021-02-01 21:57:59',0),(4,'CIM Escuela de danzas','Alicia Flores','centrointegraldelmovi@yahoo.es','2645127104','Shopping','Capital','San Juan','Argentina','2021-02-01 21:57:59',0),(5,'InstitutoTest1','Fernando Leiva','test@gmail.com','15555555','aaaaaaaaaaa','Santa Lucia','San Juan','Argentina','2021-02-01 21:57:59',0);
/*!40000 ALTER TABLE `instituciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obras`
--

DROP TABLE IF EXISTS `obras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obras` (
  `obra_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_obra` varchar(200) NOT NULL,
  `maestro_preparador` int(11) NOT NULL,
  `duracion` varchar(5) NOT NULL,
  `modalidad` varchar(100) NOT NULL,
  `estilo` varchar(100) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `forma_participacion` varchar(100) NOT NULL,
  `inst_id` int(11) NOT NULL,
  PRIMARY KEY (`obra_id`),
  KEY `obras_fk` (`inst_id`),
  CONSTRAINT `obras_fk` FOREIGN KEY (`inst_id`) REFERENCES `instituciones` (`inst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obras`
--

LOCK TABLES `obras` WRITE;
/*!40000 ALTER TABLE `obras` DISABLE KEYS */;
INSERT INTO `obras` VALUES (1,'El Cascanueces',0,'10:00','ESPAÑOL','Flamenco','Sureño','PRE-BABY','DUO',1),(2,'Taco Taco',0,'20:00','FOLKLÓRE_ARGENTINO','Danza Teatro','Looking','PROFESIONAL2','GRUPAL - (De 5 a 9 participantes)',1);
/*!40000 ALTER TABLE `obras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obras_participantes`
--

DROP TABLE IF EXISTS `obras_participantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obras_participantes` (
  `obra_id` int(11) NOT NULL,
  `alum_id` int(11) NOT NULL,
  PRIMARY KEY (`obra_id`,`alum_id`),
  KEY `obras_participantes_fk_1` (`alum_id`),
  CONSTRAINT `obras_participantes_fk` FOREIGN KEY (`obra_id`) REFERENCES `obras` (`obra_id`) ON DELETE CASCADE,
  CONSTRAINT `obras_participantes_fk_1` FOREIGN KEY (`alum_id`) REFERENCES `alumnos` (`alum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obras_participantes`
--

LOCK TABLES `obras_participantes` WRITE;
/*!40000 ALTER TABLE `obras_participantes` DISABLE KEYS */;
INSERT INTO `obras_participantes` VALUES (1,6),(1,8),(2,6),(2,7);
/*!40000 ALTER TABLE `obras_participantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `habilitado` tinyint(1) DEFAULT 1,
  `certamen` tinyint(1) DEFAULT 0,
  `capacitacion` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_un` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'fer@test.com.ar','123',1,1,1),(3,'fer17916@gmail.com','123',1,1,1),(5,'instituto1@gmail.com','123',1,0,0),(6,'ejemplo@mail.com','12345',1,0,0),(8,'test@gmail.com','111',1,0,0),(9,'admin@gmail.com','admin123',1,1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL DEFAULT 'ACTIVO',
  `ultima_sesion` datetime DEFAULT NULL,
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  `fec_alta` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `usuarios_un` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utl_tablas`
--

DROP TABLE IF EXISTS `utl_tablas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utl_tablas` (
  `tabl_id` int(11) NOT NULL AUTO_INCREMENT,
  `tabla` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `valor` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fec_alta` datetime DEFAULT current_timestamp(),
  `eliminado` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`tabl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utl_tablas`
--

LOCK TABLES `utl_tablas` WRITE;
/*!40000 ALTER TABLE `utl_tablas` DISABLE KEYS */;
INSERT INTO `utl_tablas` VALUES (1,'tipos_datos','titulo1',NULL,'2019-08-21 13:50:49',0),(2,'tipos_datos','comentario',NULL,'2019-08-21 13:50:49',0),(3,'tipos_datos','input',NULL,'2019-08-21 13:50:49',0),(4,'tipos_datos','select',NULL,'2019-08-21 13:50:49',0),(5,'tipos_datos','date',NULL,'2019-08-21 13:50:49',0),(6,'tipos_datos','check',NULL,'2019-08-21 13:50:49',0),(7,'tipos_datos','radio',NULL,'2019-08-21 13:50:49',0),(8,'tipos_datos','file',NULL,'2019-08-21 13:50:49',0),(9,'tipos_datos','textarea',NULL,'2019-08-21 13:50:49',0),(10,'provincias','San Juan',NULL,'2019-08-17 15:33:52',0),(11,'provincias','Mendoza',NULL,'2019-08-17 15:33:52',0),(12,'provincias','San Luis',NULL,'2019-08-17 15:33:52',0),(13,'sexos','Hombre',NULL,'2019-08-17 16:28:10',0),(14,'sexos','Mujer',NULL,'2019-08-17 16:28:10',0),(15,'sexos','No Binario',NULL,'2019-08-17 16:28:10',0),(16,'contratos','Acepto los Terminos y Condiciones del Servicio',NULL,'2019-08-17 17:01:22',0),(17,'contratos','Enviar Emails',NULL,'2019-08-17 17:01:22',0),(25,'tipos_datos','titulo2',NULL,'2019-09-03 23:50:28',0),(26,'tipos_datos','titulo3',NULL,'2019-09-03 23:50:28',0),(27,'sexos','Preguntale a ella',NULL,'2019-12-08 13:57:51',0),(28,'provincias','Buenos Aires',NULL,'2019-12-08 13:58:42',0),(29,'empresa_movil','Claro',NULL,'2019-12-08 13:59:25',0),(30,'forma_preparacion','SOLO',NULL,'2019-12-21 10:49:15',0),(31,'forma_preparacion','DUO',NULL,'2019-12-21 10:49:20',0),(32,'forma_preparacion','PAREJA - (Solo para Folclore y Tango)',NULL,'2019-12-21 10:51:10',0),(33,'forma_preparacion','TRIO',NULL,'2019-12-21 10:53:02',0),(34,'forma_preparacion','CUARTETO',NULL,'2019-12-21 10:53:08',0),(35,'forma_preparacion','GRUPAL - (De 5 a 9 participantes)',NULL,'2019-12-21 10:53:37',0),(36,'forma_preparacion','CONJUNTO - (Mas de 10 participantes)',NULL,'2019-12-21 10:54:02',0),(37,'categoria','BABY',NULL,'2019-12-21 11:00:19',0),(38,'categoria','PRE-INFANTIL',NULL,'2019-12-21 11:00:35',0),(39,'categoria','INFANTIL I',NULL,'2019-12-21 11:00:56',0),(40,'categoria','INFANTIL II',NULL,'2019-12-21 11:01:04',0),(41,'categoria','JUVENIL I',NULL,'2019-12-21 11:01:14',0),(42,'categoria','JUVENIL II',NULL,'2019-12-21 11:01:26',0),(43,'categoria','JUVENIL III',NULL,'2019-12-21 11:01:33',0),(44,'categoria','MAYORES I',NULL,'2019-12-21 11:01:47',0),(45,'categoria','MAYORES II',NULL,'2019-12-21 11:01:56',0),(46,'categoria','ADULTOS',NULL,'2019-12-21 11:02:04',0),(47,'categoria','PROFESIONALES',NULL,'2019-12-21 11:02:11',0);
/*!40000 ALTER TABLE `utl_tablas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cim'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05 19:29:30
