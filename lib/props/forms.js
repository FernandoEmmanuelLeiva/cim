function frmValidar(form) {
  ban = true;
  $(form).find('[required]').each(function () {
    if (this.value == '') {
      ban = false;
      $(this).addClass('is-invalid');
    }
  });
  if (!ban) $(form).find('.frm-msj').html('Complete los campos obligatorios').show();
  return ban;
}

function initForm(id = 'form', submit = true) {
  $(id).each(function () {
    $(this)
      .bootstrapValidator({
        fields: {
          select: {
            selector: ".frm-select",
            validators: {
              callback: {
                message: "Seleccionar Opción",
                callback: function (value, validator, $field) {
                  if (value == "") {
                    return false;
                  } else {
                    return true;
                  }
                },
              },
            },
          },
        },
      })
      .on("success.form.bv", function (e) {
        if (!submit) e.preventDefault();
      });
  });

  $(".date").datepicker({
    format: "dd-mm-yyyy",
    autoclose: true,
  });

  // $('input[type="radio"]').iCheck({
  //   checkboxClass: "icheckbox_flat-blue",
  //   radioClass: "iradio_flat-red",
  // });

  // $('input[type="checkbox"]').iCheck({
  //   checkboxClass: "icheckbox_flat-blue",
  //   radioClass: "iradio_flat-red",
  // });

  // $('input[type="file"]').on("change", function (e) {
  //   var filename = $(this).val();

  //   if (filename != "" && filename != null) {
  //     var link = $(this).closest(".form-group").find("a").show();
  //     var file = e.target.files[0];
  //     var filename = e.target.files[0].name;
  //     var blob = new Blob([file]);
  //     var url = URL.createObjectURL(blob);

  //     $(link).find("a").attr({
  //       download: filename,
  //       href: url,
  //     });
  //   }
  // });
}

function frmGuardar(e) {
  WaitingOpen();

  var form = $(e).closest("form").attr("id");
  var info = $(e).closest("form").data("info");

  var nuevo = info == "";
  if (nuevo) info = $(e).closest("form").data("form");

  $("#" + form).bootstrapValidator("validate");

  var bv = $("#" + form).data("bootstrapValidator");

  $("#" + form).attr("data-valido", bv.isValid() ? "true" : "false");

  var formData = new FormData($("#" + form)[0]);

  //Preparo Informacion Checkboxs
  var checkbox = $("#" + form).find("input[type=checkbox]");
  $.each(checkbox, function (key, val) {
    if (!formData.has($(val).attr("name"))) {
      formData.append($(val).attr("name"), "");
    }
  });

  //Preparo Informacion Files
  var files = $("#" + form + ' input[type="file"]');
  files.each(function () {
    if (conexion()) {
      if (this.value != null && this.value != "")
        formData.append(this.name, this.value);
    } else {
      formData.delete(this.name);
    }
  });

  var json = formToJson(formData);

  //guardarEstado($('#task').val() + '_frm', json, '#' + form);

  if (!conexion()) {
    WaitingClose();

    console.log("Offline | Formulario Guardado...");

    $("#" + form)
      .closest(".modal")
      .modal("hide");

    ajax({
      type: "POST",
      dataType: "JSON",
      url: "index.php/" + frmUrl + "Form/guardarJson/" + info,
      data: {
        json,
      },
      success: function (rsp) { },
      error: function (rsp) { },
    });
  } else {
    $.ajax({
      type: "POST",
      dataType: "JSON",
      cache: false,
      contentType: false,
      processData: false,
      url:
        "index.php/" + frmUrl + "Form/guardar/" + info + (nuevo ? "/true" : ""),
      data: formData,
      success: function (rsp) {
        $("#" + form)
          .closest(".modal")
          .modal("hide");
      },
      error: function (rsp) {
        alert("Error al Guardar Formulario");
      },
      complete: function () {
        WaitingClose();
      },
    });
  }
}

function detectarForm() {
  $(".frm-open").click(function () {
    if (isModalOpen()) return;

    obtenerForm(this.dataset.info);
  });

  $(".frm-new").click(function () {
    nuevoForm(this.dataset.form);
  });
}

function nuevoForm(form, show = true) {
  WaitingOpen();
  $.ajax({
    type: "GET",
    dataType: "JSON",
    url: "index.php/" + frmUrl + "Form/obtenerNuevo/" + form + "/" + modal,
    success: function (rsp) {
      if (modal) {
        $("#frm-list").append(rsp.html);

        if (show) $("#frm-modal-").modal("show");
        $("#frm-modal- .btn-accion").click(function () {
          $(this).closest(".modal").find(".frm-save").click();
        });
      }

      initForm();
    },
    error: function (rsp) {
      console.log("Error al Obtener Formulario");
    },
    complete: function () {
      WaitingClose();
    },
  });
}

function obtenerForm(info, show = true) {
  WaitingOpen();
  $.ajax({
    type: "GET",
    dataType: "JSON",
    url: "index.php/" + frmUrl + "Form/obtener/" + info + "/" + modal,
    success: function (rsp) {
      if (modal) {
        $("#frm-modal-" + info).remove();
        $("#frm-list").append(rsp.html);

        if (!conexion()) {
          console.log("Offiline | Sin Conexión...");

          var task = $("#task").val();
          var id = "#frm-" + info;
          var aux = JSON.parse(sessionStorage.getItem(task + "_frm"));
          if (aux != null) {
            if (aux[id] != null) {
              var form = JSON.parse(aux[id]);

              console.log("Offline | Abriendo Estado Intermedio Formulario");

              Object.keys(form).forEach(function (key) {
                //Tipo Checks
                if (key.includes("[]")) {
                  $(id + ' [name="' + key + '"]').each(function () {
                    this.checked = form[key].includes(this.value);
                  });
                } else {
                  var input = $(id + ' [name="' + key + '"]')[0];

                  //Ignorar Tipos Files
                  if (input.getAttribute("type") == "file") return;

                  //Radio
                  if (
                    input.getAttribute("type") == "radio" &&
                    input.value == form[key]
                  ) {
                    input.checked = true;
                    return;
                  }
                  console.log(input.tagName);
                  if (input.tagName == "TEXTAREA") {
                    alert("colis");
                    $(id + ' [name="' + key + '"]').html(form[key]);
                    return;
                  }

                  //Default
                  $(id + ' [name="' + key + '"]').val(form[key]);
                }
              });
            }
          }
        }
        if (show) $("#frm-modal-" + info).modal("show");
        $("#frm-modal-" + info + " .btn-accion").click(function () {
          $(this).closest(".modal").find(".frm-save").click();
        });
      }

      initForm();
    },
    error: function (rsp) {
      console.log("Error al Obtener Formulario");
    },
    complete: function () {
      WaitingClose();
    },
  });
}

function bvValidarForm(id = false) {
  $("#" + id).bootstrapValidator("validate");

  var bv = $("#" + id).data("bootstrapValidator");

  return bv.isValid();
}

function formToJson(formData) {
  var object = {};

  formData.forEach((value, key) => {
    if (!object.hasOwnProperty(key)) {
      object[key] = value;
      return;
    }

    if (!Array.isArray(object[key])) {
      object[key] = [object[key]];
    }

    object[key].push(value);
  });

  return JSON.stringify(object);
}

function getForm(form) {
  const data = new FormData($(form)[0]);
  return formToObject(data);
}

function formToObject(formData) {
  return JSON.parse(formToJson(formData));
}

function showFD(formData) {
  for (var pair of formData.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  }
}

function mergeFD(f1, f2) {
  for (var pair of f2.entries()) {
    f1.append(pair[0], pair[1]);
  }
  return f1;
}


function validar(e) {
  var ban = true;
  var aux;
  $(e).find('input').removeClass('is-invalid').removeClass('is-valid');
  $(e)
    .find(":required")
    .each(function () {
      console.log($(this).attr('name'), ' | ' ,this.value)
      aux = !(this.value == "" || this.value == null);
      $(this).addClass(aux ? 'is-valid' : 'is-invalid');
      ban = ban && aux;
    });

  $(e).find('.frm-msj').html(ban?'':'Complete los campos obligarios');
  return ban;
}

var fillForm = function (data, form = false) {
  if (!data) {
    console.log('Error al rellenar formulario');
    return;
  }
  Object.keys(data).forEach(e => {

    if (form) {

      var obj = $(form).find('[name="' + e + '"]')[0];

    } else {

      var obj = $('[name="' + e + '"]')[0];
    }

    if (!obj) return;

    switch (obj.getAttribute('type')) {
      case 'radio':
        const aux = $('[name="' + e + '"][value="' + data[e] + '"]')[0];
        aux.checked = true;
        break;
      case 'checkbox':
        obj.checked = data[e] == "1";
        $(obj).iCheck('update');
        break;

      default:
        obj.value = data[e];
        break;
    }
  });
  $('select').trigger('change');
}


function getJson(e) {
  var data = false;

  if ($(e).hasClass("json")) {
    data = $(e).attr("json");
  } else {
    data = $(e).closest(".json").attr("data-json");
  }

  if (!data) return false;
  data = JSON.parse(data);

  if (!data) return false;
  return data;
}

function formFiles(id) {
  var formData = new FormData($(id)[0]);
  var files = $(id + ' input[type="file"]');
  files.each(function () {
    if (this.value != null && this.value != "") formData.append(this.name, this.value);
  });

  return formData;
}

function frmFillFromUrl(rec, formId) {
  $.ajax({
    type: 'GET',
    dataType: 'JSON',
    url: `${rec}`,
    success: function (rsp) {
      fillForm(rsp.data, formId);
    },
    error: function () {
      error();
    }
  });
}

function frmReset(id) {
  $(id).find('.form-control').removeClass('is-invalid');
  $(id)[0].reset();
}

function frmSubmit(e , id = false, posAccion = false, p1 = false) {
  if (validar(e)) {
    var data = getForm(e);
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: $(e).attr('action') + (id?`/${id}`:''),
      data,
      success: function (res) {
        if(res.status){
          hecho();
          $('.modal').modal('hide');
          if(posAccion) posAccion(p1);
        }

      },
      error: function (res) {
        error();
      },
      complete: function () {
        wb()
      }
    })
  }else wb();
}