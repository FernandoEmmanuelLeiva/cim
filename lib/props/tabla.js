function DataTable(tabla, columnas = null, sort = null) {

    var accion = [{
            "targets": columnas,
            "searchable": false
        },
        {
            "targets": columnas,
            "orderable": false
        }
    ];
    if (sort != null) sort = [sort];
    else sort = [];

    var aux = $(tabla).DataTable();
    if (aux != null) aux.destroy();

    $(tabla).DataTable({
        "paging": false,
        "columnDefs": accion,
        "order": sort,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "_START_ de _END_ | Total Registros _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $('.dataTables_filter').hide();
}

function filtroTabla(tabla, input) {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(input);
    filter = input.value.toUpperCase();

    if(filter.length == 0){
      $('#showAll').hide()
    }else{
      $('#showAll').show();
     
    }

    table = document.getElementById(tabla);
    tr = table.getElementsByTagName("tr");
  
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }

