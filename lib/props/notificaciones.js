var Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 3000,
});

function hecho(titulo = 'Guardado con Éxito', msj = "") {
    Swal.fire(
        titulo,
        msj,
        'success'
    );
}
function n_hecho(msj = "Operación Exitosa") {
    Toast.fire({
        type: 'success',
        title: msj
    });
}

function r_hecho(msj = "Hecho", detalle = "Guardado con Éxito!") {
    Swal.fire(msj, detalle, "success").then((result) => {
        $(".modal-backdrop ").remove();
        if (result.value) linkTo();
    });
}

function ajaxError(error) {
    errorAlert('Ups!', error.responseJSON?error.responseJSON.mensaje:false);
}

function errorAlert(msj = "Error!", detalle = "Algo salio mal") {
    Swal.fire({
        type: 'error',
        title: msj,
        text: detalle
    });
}

function falla(msj = "Fallo!", detalle = "") {
    Swal.fire({
        type: 'warning',
        title: msj,
        text: detalle
    });
}

function conf(fun, e, title = "Confirma Eliminacíon?", text = "Esta acción no se pordrá revertir") {
    Swal.fire({
        title,
        text,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
    }).then((result) => {
        if (result.value) {
            fun(e);
        }
    });
}

function userAlert(msj){
    alert(msj);
}


function info(msj, detalle = "") {
    Swal.fire({
        type: 'info',
        title: msj,
        text: detalle
    });
}