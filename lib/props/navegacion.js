var site = null;
var backSite = null;

var linkTo = function (link = null, recipiente = "#content") {
    wo();
    $(".modal-backdrop").remove();
    $("body").removeClass("modal-open");
    $(recipiente).empty();

    if (link != null) {
        $(recipiente).load(link, function() {
            wc();
        });
        backSite = site;
        site = link;
        return;
    }

    $(recipiente).load(site);
    wc();
}

function redirect(link){
    window.location.href = link;
}

function loadPage(link, recipiente) {
    wo();
    $(recipiente).empty();
    $(recipiente).load("index.php/" + link);
    wc();
}

function back() {
    linkTo(backSite);
}
$(".menu-link").click(function() {
    pushMenu();
    $("#link-page").html(this.dataset.nombre);
    if (this.dataset.link == "") {
        linkTo("Admin/nopage");
        return;
    }
    linkTo(this.dataset.link);
});

function wo(texto) {
    if (texto == "" || texto == null) {
        $("#waitingText").html("Cargando ...");
    } else {
        $("#waitingText").html(texto);
    }
    $("#waiting").fadeIn("slow");
}

function wc() {
    $("#waiting").fadeOut("slow");
}
var bak_icon = null;
var wait_icon = '<i class="fa fa-spinner fa-spin mr-1"></i>';
var backButton = false;
function wb(e = false) {
    if(!e) e = backButton;
    else backButton = e;

    var ban = $(e).prop("disabled");
    if (ban) {
        if ($(e).hasClass("fa")) {
            $(e).removeClass().addClass(bak_icon);
        } else {
            $(e).find("i").remove();
            $(e).prepend("<i class='" + bak_icon + "'></i>");
        }
    } else {
        if ($(e).hasClass("fa")) {
            bak_icon = $(e).attr("class");
            $(e).removeClass().addClass("fa fa-spinner fa-spin mr-1");
        } else {
            bak_icon = $(e).find("i").attr("class");
            $(e).find("i").remove();
            $(e).prepend(wait_icon);
        }
    }
    $(e).prop("disabled", !ban);
}


var reload = function(e = false, parametro = false, fn = false) {
    if (e) {
        var link = false;
        if ($(e).hasClass("reload")) {
            link = $(e).attr("data-link");
        } else {
            e = $(e).closest(".reload");
            link = $(e).closest(".reload").attr("data-link");
        }

        if (!link) {
            console.log("Falla al recargar el contenido");
            return;
        }

        //$(e).html("Cargando...");
        $(e).load(link + (parametro ? "/" + parametro : ""), () => {
            if (fn) fn();
        });
    } else {
        return;
    }
}
var wm_html =
    '<div class="overlay dark d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>';

var wm = function(id = false) {
    if (id) {
        $(id).find(".modal-content").append(wm_html);
    } else {
        $(".overlay").remove();
    }
}

function wbox(id = false) {
    if (id) {
        $(id).append(wm_html);
    } else {
        $(".overlay").remove();
    }
}

var wicon = "fa fa-spinner fa-spin w-icon";

function wIcon(id) {
    if (id) {
        bak_icon = $(id).attr('class');
        $(id).removeClass().addClass(wicon);
    } else {
        $('.w-icon').removeClass().addClass(bak_icon);
    }
}